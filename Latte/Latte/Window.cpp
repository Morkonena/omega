#include <GL.h>
#include <Log.h>

#include <GL\wglew.h>
#include <sstream>
#include <Windows.h>

#include "Graphics.h"
#include "Window.h"

#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

using namespace Core;

std::atomic_int Window::window_count;

Vector2 motion;

extern LRESULT CALLBACK OnWindowEvent(HWND window_handle, UINT message, WPARAM w, LPARAM l)
{
  if (message == WM_INPUT)
  {
    RAWINPUT input;
    UINT total_size = sizeof(RAWINPUT);
    UINT header_size = sizeof(RAWINPUTHEADER);

    GetRawInputData((HRAWINPUT)l, RID_INPUT, &input, &total_size, header_size);

    if (input.header.dwType == RIM_TYPEMOUSE)
    {
      motion.x = (float)input.data.mouse.lLastX;
      motion.y = (float)input.data.mouse.lLastY;
    }
  }

  return DefWindowProc(window_handle, message, w, l);
}

void Window::Process()
{
  MSG msg = {};

  // Process all messages until there's none
  while (PeekMessage(&msg, window_handle, 0, 0, PM_REMOVE) > 0)
  {
    DispatchMessage(&msg);
  }
}

Window::Window(int width, int height, bool fullscreen, std::string title)
{
  this->width = width;
  this->height = height;

  // Get handle to this application
  HINSTANCE instance = GetModuleHandle(0);

  // Create unique name for this window
  std::stringstream stream;
  stream << "Latte (" << window_count++ << ")";

  // Save the name for later use
  name = stream.str();

  // Create window settings
  WNDCLASSA window_info = {};
  window_info.hbrBackground = CreateSolidBrush(RGB(255, 255, 255));
  window_info.hInstance = instance;
  window_info.lpfnWndProc = OnWindowEvent;
  window_info.lpszClassName = name.c_str();
  window_info.style = CS_OWNDC;

  // Register this window
  if (!RegisterClass(&window_info))
  {
    PERROR("ERROR_REGISTER_WINDOW");
    throw;
  }

  // Create this window
  window_handle = CreateWindow(name.c_str(), title.c_str(), fullscreen ? WS_POPUP : WS_OVERLAPPEDWINDOW, 0, 0, width, height, 0, 0, instance, 0);

  if (!window_handle)
  {
    PERROR("ERROR_CREATE_WINDOW");
    throw;
  }

  // Retrieve device context for this window
  device_context = GetDC(window_handle);
  
  if (!device_context)
  {
    PERROR("ERROR_DEVICE_CONTEXT");
    throw;
  }

  // Create settings for pixel format
  PIXELFORMATDESCRIPTOR format_descriptor = {};
  format_descriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  format_descriptor.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
  format_descriptor.iPixelType = PFD_TYPE_RGBA;
  format_descriptor.cColorBits = 32;
  format_descriptor.cDepthBits = 24;
  format_descriptor.cStencilBits = 8;

  // Create settings for pixel format
  const int format_attributes[] =
  {
    WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
    WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
    WGL_DOUBLE_BUFFER_ARB,  GL_TRUE,
    WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
    WGL_COLOR_BITS_ARB, 32,
    WGL_DEPTH_BITS_ARB, 24,
    WGL_STENCIL_BITS_ARB, 8,
    0
  };
  
  int format;
  unsigned int format_count;

  // Choose a pixel format based on the settings
  if (wglChoosePixelFormatARB(device_context, format_attributes, 0, 1, &format, &format_count) != TRUE)
  {
    PERROR("ERROR_CHOOSE_PIXEL_FORMAT");
    throw;
  }

  // Set the pixel format to the chosen one
  if (SetPixelFormat(device_context, format, &format_descriptor) != TRUE)
  {
    PERROR("ERROR_SET_PIXEL_FORMAT");
    throw;
  }

  // Initialize the rendering context
  rendering_context = RenderingContext(this);

  RAWINPUTDEVICE mouse = {};
  mouse.usUsagePage = HID_USAGE_PAGE_GENERIC;
  mouse.usUsage = HID_USAGE_GENERIC_MOUSE;
  mouse.dwFlags = RIDEV_INPUTSINK;
  mouse.hwndTarget = window_handle;

  // Register mouse
  if (RegisterRawInputDevices(&mouse, 1, sizeof(RAWINPUTDEVICE)) != TRUE)
  {
    PERROR("ERROR_REGISTER_MOUSE (", GetLastError(), ")");
    throw;
  }

  Process();
}

Window::~Window()
{
  DestroyWindow(window_handle);
}

void Window::Start()
{
	Show();

	if (!GetRenderingContext().Bind())
	{
		PERROR("ERROR_START_WINDOW");
		throw;
	}
}

void Window::Show()
{
  ShowWindow(window_handle, SW_SHOW);
}

void Window::Hide()
{
  ShowWindow(window_handle, SW_HIDE);
}

void Window::SetCursorPosition(Vector2 position)
{
  SetCursorPos((int)position.x, (int)position.y);
}

Vector2 Window::GetMouseMovement() const
{
  return motion;
}

void Window::SetCursorVisible(bool visible)
{
  ShowCursor(visible);
}

void Window::SetCursorLocked(bool locked)
{
  this->is_locked = locked;
}

int Window::GetWidth() const
{
  return width;
}

int Window::GetHeight() const
{
  return height;
}

Vector2 Window::GetCursorPosition() const
{
  POINT point = {};
  GetCursorPos(&point);

  return Vector2(float(point.x), float(point.y));
}

DeviceContext Window::GetDeviceContext() const
{
  return device_context;
}

RenderingContext& Window::GetRenderingContext() const
{
  return rendering_context;
}

bool Window::IsKeyDown(int key) const
{
  return (GetKeyState(key) & 0x8000) == 0x8000;
}

bool Window::IsClosing() const
{
  return is_closing;
}

void Window::Update()
{
  SwapBuffers(device_context);

  motion = Vector2();
  Process();

  if (is_locked)
  {
    SetCursorPosition(Vector2(width / 2.0f, height / 2.0f));
  }
}
