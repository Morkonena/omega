//
// Created by M�rk�nen� on 10.7.2018
//

#ifndef CORE_WINDOW_H
#define CORE_WINDOW_H

#include <atomic>
#include <string>

#include "RenderingContext.h"
#include "Vector2.h"

typedef struct HWND__* WindowHandle;
typedef struct HDC__* DeviceContext;

namespace Core
{
  class Window
  {
  private:
    static std::atomic_int window_count;

    int width;
    int height;

    std::string name;

    mutable WindowHandle window_handle = 0;
    mutable DeviceContext device_context = 0;
    mutable RenderingContext rendering_context;

    bool is_locked = false;
    bool is_closing = false;

    void Process();

  public:
    Window(int width, int height, bool fullscreen = true, std::string title = "");
    ~Window();

		void Start();
    void Show();
    void Hide();

    void SetCursorVisible(bool visible);
    void SetCursorLocked(bool locked);
    void SetCursorPosition(Vector2 position);

    Vector2 GetMouseMovement() const;

    int GetWidth() const;
    int GetHeight() const;

    Vector2 GetCursorPosition() const;

    DeviceContext GetDeviceContext() const;
    RenderingContext& GetRenderingContext() const;

    bool IsKeyDown(int key) const;
    bool IsClosing() const;

    void Update();
  };
}

#endif // !CORE_WINDOW_H