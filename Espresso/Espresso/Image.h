//
// Created by M�rk�nen� on 18.7.2018
//

#ifndef CORE_IMAGE_H
#define CORE_IMAGE_H

#include <Standard.h>
#include <Texture.h>

namespace Core
{
	enum ImageFileFormat
	{
		IMAGE_FORMAT_PNG,
		IMAGE_FORMAT_JPG,
		IMAGE_FORMAT_BMP
	};

  class Image
  {
  public:
		static void Initialize();
    static Texture* Import (std::string filename, ImageFileFormat format);
  };
}

#endif // !CORE_IMAGE_H