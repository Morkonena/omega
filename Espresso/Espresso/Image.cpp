#include "Image.h"

#include <Asset.h>
#include <Log.h>
#include <FreeImage.h>

using namespace Core;

FREE_IMAGE_FORMAT image_formats[] =
{
	FIF_PNG,
	FIF_JPEG,
	FIF_BMP
};

void Image::Initialize()
{
	FreeImage_Initialise();
}

Texture* Image::Import (std::string filename, ImageFileFormat image_format)
{
	// Load the image as asset
	Asset asset(filename);

	// Create bitmap from the asset
	FIMEMORY memory = { asset.GetData() };	
	FIBITMAP* bitmap = FreeImage_LoadFromMemory(image_formats[(int)image_format], &memory);

	// Make sure bitmap is valid
	if (!bitmap)
	{
		PERROR("ERROR_IMPORT_IMAGE");
		return nullptr;
	}

	// Support for larger bit sizes will be added
	if (FreeImage_GetRedMask(bitmap)   != 8 ||
		  FreeImage_GetGreenMask(bitmap) != 8 ||
		  FreeImage_GetBlueMask(bitmap)  != 8)
	{
		PERROR("ERROR_UNSUPPORTED_IMAGE_TYPE: (Only 8 bits per channel is supported)");
		return nullptr;
	}

	// Determine texture format
	TextureFormat format;
	uint32_t component_count;

	switch (FreeImage_GetColorType(bitmap))
	{
	case FIC_RGB:
		format = TEXTURE_FORMAT_RGB8;
		component_count = 3;
		break;
	case FIC_RGBALPHA:
		format = TEXTURE_FORMAT_RGBA8;
		component_count = 4;
		break;
	case FIC_MINISWHITE:
	case FIC_MINISBLACK:
	case FIC_PALETTE:
	case FIC_CMYK:
	default:
		PERROR("ERROR_UNSUPPORTED_IMAGE_TYPE: (Only RGB/RGBA is supported)");
		return nullptr;
	}

	// Tells image's dimensions
	uint32_t width = FreeImage_GetWidth(bitmap);
	uint32_t height = FreeImage_GetHeight(bitmap);

	// Copy all scanlines to this buffer
	std::vector<uint8_t> pixels(width * height * component_count);
	std::uint32_t scanline_size = width * component_count;

	for (uint32_t i = 0; i < height; i++)
	{
		auto src = FreeImage_GetScanLine(bitmap, i);
		auto dst = static_cast<uint8_t*>(pixels.data()) + (scanline_size * i);
		
		// Copy current scanline to the buffer
		memcpy(dst, src, scanline_size);
	}

	Texture* texture = new Texture();
	
	// Make sure the texture is valid
	if (!texture->IsValid())
	{
		return nullptr;
	}
	
	// Upload the pixels to the texture
	texture->SetPixels(format, width, height, pixels.data());
	
  return texture;
}
