#include "SpecularMaterial.h"
#include "DefferedRenderer.h"

#include <Actor.h>
#include <Asset.h>
#include <Camera.h>
#include <Log.h>
#include <Mesh.h>

using namespace Core;

struct SpecularMaterialProperties
{
	Vector3 Diffuse;
	Vector3 Specular;
	float Power;
};

SpecularMaterial::SpecularMaterial(Actor* Parent) : Material(Parent)
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	Properties.Diffuse = Vector3(0.8f);
	Properties.Power = 100.0f;
	Properties.Specular = Vector3(1.0f);
}

void SpecularMaterial::SetDiffuseColor(Vector3 Diffuse)
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	Properties.Diffuse = Diffuse;
}

void SpecularMaterial::SetSpecularColor(Vector3 Specular)
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	Properties.Specular = Specular;
}

void SpecularMaterial::SetPower(float Power)
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	Properties.Power = Power;
}

Vector3 SpecularMaterial::GetDiffuseColor() const
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	return Properties.Diffuse;
}

Vector3 SpecularMaterial::GetSpecularColor() const
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	return Properties.Specular;
}

float SpecularMaterial::GetPower() const
{
	SpecularMaterialProperties& Properties = GetMaterialProperties<SpecularMaterialProperties>();
	return Properties.Power;
}

void SpecularMaterial::OnTick()
{
  /*if (mesh != nullptr && GetEnabled(true))
  {
		Effect* effect = DefferedRenderer::GetFillEffect();

		effect->Use();
    mesh->Use();

    if (texture != nullptr)
    {
      texture->Use();
    }

    Matrix m = GetParent()->GetWorldMatrix();
    Matrix mvp = Camera::GetProjection() * Camera::GetView() * m;

		effect->Set("mid", effect->GetIdentity());
    effect->Set("mvp", mvp);
    effect->Set("m", m);

		if (mesh->HasBuffer(BUFFER_TARGET_INDICES))
		{
			Buffer* buffer = mesh->GetBuffer(BUFFER_TARGET_INDICES);
			Camera::Draw(DRAW_MODE_TRIANGLES, true, buffer->GetSize() / sizeof(int));
		}
		else
		{
			Buffer* buffer = mesh->GetBuffer(BUFFER_TARGET_VERTICES);
			Camera::Draw(DRAW_MODE_TRIANGLES, false, buffer->GetSize() / sizeof(Vector3));
		}
  }*/
}

Type SpecularMaterial::GetType()
{
  return typeid(SpecularMaterial);
}

void SpecularMaterial::Initialize()
{
	Material::Initialize<SpecularMaterialProperties>(1000);

  Shader* Vs = new Shader(SHADER_TYPE_VERTEX, Asset("Shaders/Deffered/Specular/Shader.vs").GetText());

  if (!Vs->IsValid())
  {
    PERROR("ERROR_SPECULAR_VERTEX_SHADER: ", Vs->GetLog().c_str());
    throw;
  }

  Shader* Fs = new Shader(SHADER_TYPE_FRAGMENT, Asset("Shaders/Deffered/Specular/Shader.fs").GetText());
  
  if (!Fs->IsValid())
  {
    PERROR("ERROR_SPECULAR_FRAGMENT_SHADER: ", Fs->GetLog().c_str());
    throw;
  }

	Effect = std::unique_ptr<Core::Effect>(new Core::Effect({ Vs, Fs }, 2));

  if (!Effect->IsValid())
  {
    PERROR("ERROR_SPECULAR_EFFECT: ", Effect->GetLog().c_str());
    throw;
  }

	Effect->AddSampler("materials");
	Effect->AddSampler("positions");
	Effect->AddSampler("normals");
	Effect->AddSampler("colors");

  SUCCESS("Deffered/Specular: OK");
}
