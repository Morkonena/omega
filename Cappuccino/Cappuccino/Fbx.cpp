#include "Fbx.h"
#include "SpecularMaterial.h"

#include <Actor.h>
#include <Asset.h>
#include <Log.h>
#include <Mesh.h>
#include <Vector2.h>
#include <Vector3.h>

#include <fbxsdk.h>

using namespace Core;

#define CAST(v) Vector3(v[0], v[1], v[2]);

FbxManager* Fbx::manager;

std::unique_ptr<Mesh> CreateMesh(FbxMesh* fbx)
{
	if (!fbx->IsTriangleMesh())
	{
		PERROR("ERROR_NON_TRIANGLE_MESH");
		throw;
	}

  int polygon_count = fbx->GetPolygonCount();
  int vertex_count = polygon_count * 3;

	std::vector<Vector3> vertices(vertex_count);
	std::vector<Vector3> normals(vertex_count);
	std::vector<Vector2> uvs;

	FbxStringList uv_maps;
	fbx->GetUVSetNames(uv_maps);

	if (uv_maps.GetCount() > 0)
	{
		uvs.reserve(vertex_count);
	}

  FbxVector4 position;
  FbxVector4 normal;
  FbxVector2 uv;

	bool mapped;

  for (int i = 0; i < polygon_count; i++)
  {
    for (int l = 0; l < 3; l++)
    {
      position = fbx->GetControlPointAt(fbx->GetPolygonVertex(i, l));

      if (!fbx->GetPolygonVertexNormal(i, l, normal))
      {
				normal = FbxVector4();
      }

      if (uv_maps.GetCount() > 0)
      {
				if (!fbx->GetPolygonVertexUV(i, l, uv_maps[0], uv, mapped) || !mapped)
				{
					uv = FbxVector2(-1.0, -1.0);
				}
				
				uvs[i * 3 + l] = Vector2(uv[0], uv[1]);
      }
			
			vertices[i * 3 + l] = Vector3(position[0], position[1], position[2]);
			normals[i * 3 + l] = Vector3(normal[0], normal[1], normal[2]);
    }
  }
	
	auto mesh = std::make_unique<Mesh>();
  mesh->CreateBuffer(BUFFER_TARGET_VERTICES).SetData(vertices.data(), vertex_count);
  mesh->CreateBuffer(BUFFER_TARGET_NORMALS).SetData(normals.data(), vertex_count);
  mesh->CreateBuffer(BUFFER_TARGET_UVS, BUFFER_TARGET_INPUT_VECTOR2).SetData(uvs.data(), vertex_count);
  
	if (!mesh->Upload())
	{
		PERROR("ERROR_CREATE_MESH");
		throw;
	}

  return mesh;
}

std::unique_ptr<Actor> CreateActorFromNode(FbxNode* node)
{
  FbxNodeAttribute* attribute = node->GetNodeAttribute();

	std::unique_ptr<Actor> actor = std::make_unique<Actor>();
  actor->SetName(std::string(node->GetName()));
  actor->position	= CAST(node->LclTranslation.Get());
  actor->rotation	= CAST(node->LclRotation.Get());
  actor->scale = CAST(node->LclScaling.Get());

  if (attribute != 0)
  {
    FbxNodeAttribute::EType type = attribute->GetAttributeType();

    if (type == FbxNodeAttribute::eMesh)
    {
      std::unique_ptr<Mesh> mesh = CreateMesh(node->GetMesh());
			
      /// TODO: Material Loading
      SpecularMaterial* material = actor->AddComponent<SpecularMaterial>();
			material->SetMesh(mesh.release());
    }
    else if (type == FbxNodeAttribute::eLight)
    {
      /// TODO: Light Support
    }
  }

	return actor;
}

void CreateChildNode(FbxNode* child_node, Actor* parent)
{
  std::unique_ptr<Actor> child = CreateActorFromNode(child_node);
  child->SetParent(parent);

  for (int i = 0; i < child_node->GetChildCount(); i++)
  {
    CreateChildNode(child_node->GetChild(i), child.get());
  }

	child.release();
}

Actor* Fbx::Import(std::string filename)
{
  // Create the importer
  FbxImporter* importer = FbxImporter::Create(manager, "");

  // Assemble the file path
  std::string path = Assets::GetFolder() + "/" + filename;

  // Prepare to import
  if (!importer->Initialize(path.c_str()))
  {
    PERROR("ERROR_CREATE_FBX_IMPORTER (", filename, ")");
    throw;
  }

  // Create a scene
  FbxScene* scene = FbxScene::Create(manager, "");

  // Import the file to the scene
  if (!importer->Import(scene))
  {
    PERROR("ERROR_IMPORT_FBX (", filename, ")");
    throw;
  }

  // Importer is no longer needed
  importer->Destroy();
	
  // Create a tool for triangulating
  FbxGeometryConverter triangulator(manager);
	
  // Convert the scene into triangles
  if (!triangulator.Triangulate(scene, true))
  {
    PERROR("ERROR_TRIANGULATE_FBX (", filename, ")");
    throw;
  }

  // Create the root node
  auto root_node = scene->GetRootNode();
  auto root_actor = CreateActorFromNode(root_node);

	// Create all root node's childs
  for (int i = 0; i < root_node->GetChildCount(); i++)
  {
    CreateChildNode(root_node->GetChild(i), root_actor.get());
  }

  return root_actor.release();
}

void Fbx::Initialize()
{
	manager = FbxManager::Create();
	SUCCESS("Fbx: OK");
}

