//
// Created by M�rk�nen� on 29.9.2018
//

#ifndef CORE_DEFFERED_RENDERER_H
#define CORE_DEFFERED_RENDERER_H

#include <Effect.h>
#include <Framebuffer.h>
#include <Mesh.h>
#include <Texture.h>
#include <Window.h>

namespace Core
{
	class Deffered
	{
	private:
		friend class Material;

		static int Width;
		static int Height;

		static Vector3 Background;

		static std::unique_ptr<Framebuffer> Framebuffer;
		static std::unique_ptr<Texture> DepthStencilBuffer;

		static std::unique_ptr<Mesh> Quad;
		
		static std::unique_ptr<Effect> Combine;
		static std::unique_ptr<Effect> Fill;

		static std::vector<void(*)()> Materials;

		static void CreateQuad();
		static void CreateCombineEffect();
		static void CreateFillEffect();

		template<class T>
		static void(*GetMaterialRenderFunction())();

	public:
		static void Initialize(const Window& window);

		static void SetBackgroundColor(Vector3 color);

		template<class T>
		static bool IsRegistered();

		template<class T>
		static void Register();

		static void Render();

		static Effect* GetFillEffect();
	};

	template<class T>
	inline void(*Deffered::GetMaterialRenderFunction())()
	{
		return T::Render;
	}

	template<class T>
	inline bool Deffered::IsRegistered()
	{
		return std::find(Materials.begin(), Materials.end(), GetMaterialRenderFunction<T>());
	}

	template<class T>
	inline void Deffered::Register()
	{
		Materials.push_back(GetMaterialRenderFunction<T>());
	}
}

#endif // !CORE_DEFFERED_RENDERER_H