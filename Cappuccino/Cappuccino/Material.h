//
// Created by M�rk�nen� on 12.7.2018
//

#ifndef CORE_MATERIAL_H
#define CORE_MATERIAL_H

#include <Component.h>
#include <Mesh.h>
#include <Texture.h>
#include <Framebuffer.h>

#include <queue>

namespace Core
{
	template<class X>
  class Material : public Component
  {
	private:
		static std::unique_ptr<Framebuffer> Framebuffer;

		static std::unique_ptr<Texture> Identities;
		static std::unique_ptr<Texture> Positions;
		static std::unique_ptr<Texture> Normals;
		static std::unique_ptr<Texture> Colors;

	private:
		static std::unique_ptr<Buffer> Materials;
		static std::uint32_t MaterialSize;

		static std::queue<uint32_t> Slots;

		static std::uint8_t* Mapped;
		static std::uint32_t Count;

		static std::vector<Material*> Batch;
		static std::uint32_t Max;

	private:
		static void Initialize(int MaterialSize, int BatchCapacity)
		{
			X::MaterialSize = MaterialSize;
			X::Max = BatchCapacity;

			X::Materials = std::make_unique<Buffer>(BUFFER_TYPE_SHADER, BUFFER_USAGE_DYNAMIC);
			X::Materials->SetData(nullptr, MaterialSize * BatchCapacity);
			X::Mapped = static_cast<uint8_t*>(X::Materials->Map(BUFFER_ACCESS_BOTH));

			X::Framebuffer = std::make_unique<Core::Framebuffer>();

			X::Identities = std::make_unique<Texture>();
			X::Identities->SetPixels(TEXTURE_FORMAT_INT, Deffered::Width, Deffered::Height, nullptr);
			X::Identities->SetSmoothing(false);

			X::Positions = std::make_unique<Texture>();
			X::Positions->SetPixels(TEXTURE_FORMAT_RGB32, Deffered::Width, Deffered::Height, nullptr);
			X::Positions->SetSmoothing(false);

			X::Normals = std::make_unique<Texture>();
			X::Normals->SetPixels(TEXTURE_FORMAT_RGB32, Deffered::Width, Deffered::Height, nullptr);
			X::Normals->SetSmoothing(false);

			X::Colors = std::make_unique<Texture>();
			X::Colors->SetPixels(TEXTURE_FORMAT_RGB8, Deffered::Width, Deffered::Height, nullptr);
			X::Colors->SetSmoothing(false);

			X::Framebuffer->Attach(Identities.get(), FRAMEBUFFER_OUTPUT_COLOR + 0);
			X::Framebuffer->Attach(Positions.get(), FRAMEBUFFER_OUTPUT_COLOR + 1);
			X::Framebuffer->Attach(Normals.get(), FRAMEBUFFER_OUTPUT_COLOR + 2);
			X::Framebuffer->Attach(Colors.get(), FRAMEBUFFER_OUTPUT_COLOR + 3);

			Framebuffer->Attach(Deffered::DepthStencilBuffer.get(), FRAMEBUFFER_OUTPUT_DEPTH);

			if (!Framebuffer->IsValid())
			{
				Framebuffer->Log();

				PERROR("ERROR_CREATE_MATERIAL_FRAMEBUFFER");
				throw;
			}
		}
	
	private:
		void* GetMaterialProperties() const;
		int Slot;

	private:
		friend class Deffered;

		/// Renders all users of this material
		static void Render()
		{
			// Commit material changes to gpu
			X::Materials->Unmap();

			// Fill-Stage
			{
				Camera::Begin();
				X::Framebuffer->Use();

				// Clear material's framebuffer, but keep the depthbuffer since it's shared
				Camera::Clear(Vector3(), CLEAR_BACKGROUND);

				// Bind the fill effect
				Core::Effect& fill = *Deffered::Fill;
				fill->Use();

				// Render all users of this material
				for (auto& material : X::Batch)
				{
					// Bind material's mesh
					Mesh* mesh = material->mesh;
					mesh->Use();

					// Bind material's texture if it's set
					Texture* texture = material->texture;

					if (texture)
					{
						texture->Use();
					}

					// Calculate model and model-view-projection matrices
					Matrix m = material->GetParent()->GetWorldMatrix();
					Matrix mvp = Camera::GetProjection() * Camera::GetView() * m;

					// Transfer material id and the matrices to the shader
					fill->Set("material", material->Slot);
					fill->Set("mvp", mvp);
					fill->Set("m", m);

					// Decide whether rendering is done in indexed mode or not
					if (mesh->HasBuffer(BUFFER_TARGET_INDICES))
					{
						Buffer* indices = mesh->GetBuffer(BUFFER_TARGET_INDICES);
						Camera::Draw(DRAW_MODE_TRIANGLES, true, indices->GetSize() / sizeof(int));
					}
					else
					{
						Buffer* vertices = mesh->GetBuffer(BUFFER_TARGET_VERTICES);
						Camera::Draw(DRAW_MODE_TRIANGLES, false, vertices->GetSize() / sizeof(Vector3));
					}
				}

				// Fill-Stage is now complete
				Camera::End();
			}

			// Deffered-Stage
			{
				Camera::Begin();
				Camera::Clear(Vector3(), CLEAR_BOTH);

				Deffered::Quad->Use();

				// Prepare the effect
				X::Effect->Use();
				X::Effect->Set("dl_position", Vector3(0, 0, 0));
				X::Effect->Set("dl_direction", Vector3(-1, 0, 0));
				X::Effect->Set("camera_position", Camera::position);
				X::Effect->Set("ambient", Deffered::Background * 0.66f);

				// Bind the material buffer
				X::Effect->SetShaderBuffer(*X::Materials, 0);

				// Bind the different render textures
				X::Identities->Use(X::Effect->GetSampler("materials"));
				X::Positions->Use(X::Effect->GetSampler("positions"));
				X::Normals->Use(X::Effect->GetSampler("normals"));
				X::Colors->Use(X::Effect->GetSampler("colors"));

				Camera::Draw(DRAW_MODE_TRIANGLES, false, 6);
				Camera::End();
			}

			X::Mapped = static_cast<uint8_t*>(X::Materials->Map(BUFFER_ACCESS_BOTH));
		}

  protected:
		static std::unique_ptr<Effect> Effect;

		template<typename T>
		static void Initialize(int BatchCapacity);

		template<typename T>
		T& GetMaterialProperties() const;

    Texture* texture;
		Mesh* mesh;

  public:
		Material::Material(Actor* parent) : Component(parent)
		{
			if (!X::Slots.empty())
			{
				Slot = X::Slots.front();
				X::Slots.pop();

				X::Batch.push_back(this);
			}
			else if (X::Count < X::Max)
			{
				Slot = X::Count++;
				X::Batch.push_back(this);
			}
			else
			{
				PERROR("ERROR_BATCH_FULL");
				throw;
			}
		}

    void			SetTexture	(Texture* texture);
    Texture*	GetTexture	() const;

		void			SetMesh			(Mesh* mesh);
		Mesh*			GetMesh			() const;

		void OnDestroy() override;
  };

	template<typename T>
	inline void Material::Initialize (int BatchCapacity)
	{
		Initialize(sizeof(T), BatchCapacity);
	}

	template<typename T>
	inline T& Material::GetMaterialProperties() const
	{
		return *static_cast<T*>(GetMaterialProperties());
	}
}

#endif // !CORE_MATERIAL_H