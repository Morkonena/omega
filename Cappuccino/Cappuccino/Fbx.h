//
// Created by M�rk�nen� on 9.7.2018
//

#ifndef CORE_FBX_H
#define CORE_FBX_H

#include <Standard.h>

namespace fbxsdk
{
	class FbxManager;
}

namespace FbxSdk = fbxsdk;

namespace Core
{
  class Actor;

	class Fbx
	{
	private:
		static FbxSdk::FbxManager* manager;

	public:
		static Actor* Import(std::string filename);
		static void		Initialize();
	};
}

#endif // !CORE_FBX_H