#include "Material.h"
#include "DefferedRenderer.h"

#include <Actor.h>
#include <Log.h>

using namespace Core;

std::unique_ptr<Core::Framebuffer> Material::Framebuffer;

std::unique_ptr<Texture> Material::Identities;
std::unique_ptr<Texture> Material::Positions;
std::unique_ptr<Texture> Material::Normals;
std::unique_ptr<Texture> Material::Colors;

std::unique_ptr<Buffer> Material::Materials;
std::uint32_t Material::MaterialSize;

std::queue<uint32_t> Material::Slots;

std::uint8_t* Material::Mapped;
std::uint32_t Material::Count;

std::vector<Material*> Material::Batch;
std::uint32_t Material::Max;

std::unique_ptr<Effect> Material::Effect;

void Material::Initialize(int MaterialSize, int BatchCapacity)
{
	Material::MaterialSize = MaterialSize;
	Material::Max = BatchCapacity;

	Materials = std::make_unique<Buffer>(BUFFER_TYPE_SHADER, BUFFER_USAGE_DYNAMIC);
	Materials->SetData(nullptr, MaterialSize * BatchCapacity);
	Mapped = static_cast<uint8_t*>(Materials->Map(BUFFER_ACCESS_BOTH));

	Framebuffer = std::make_unique<Core::Framebuffer>();

	Identities = std::make_unique<Texture>();
	Identities->SetPixels(TEXTURE_FORMAT_INT, Deffered::Width, Deffered::Height, nullptr);
	Identities->SetSmoothing(false);

	Positions = std::make_unique<Texture>();
	Positions->SetPixels(TEXTURE_FORMAT_RGB32, Deffered::Width, Deffered::Height, nullptr);
	Positions->SetSmoothing(false);

	Normals = std::make_unique<Texture>();
	Normals->SetPixels(TEXTURE_FORMAT_RGB32, Deffered::Width, Deffered::Height, nullptr);
	Normals->SetSmoothing(false);

	Colors = std::make_unique<Texture>();
	Colors->SetPixels(TEXTURE_FORMAT_RGB8, Deffered::Width, Deffered::Height, nullptr);
	Colors->SetSmoothing(false);

	Framebuffer->Attach(Identities.get(), FRAMEBUFFER_OUTPUT_COLOR + 0);
	Framebuffer->Attach(Positions.get(), FRAMEBUFFER_OUTPUT_COLOR + 1);
	Framebuffer->Attach(Normals.get(), FRAMEBUFFER_OUTPUT_COLOR + 2);
	Framebuffer->Attach(Colors.get(), FRAMEBUFFER_OUTPUT_COLOR + 3);

	Framebuffer->Attach(Deffered::DepthStencilBuffer.get(), FRAMEBUFFER_OUTPUT_DEPTH);

	if (!Framebuffer->IsValid())
	{
		Framebuffer->Log();

		PERROR("ERROR_CREATE_MATERIAL_FRAMEBUFFER");
		throw;
	}
}

void* Material::GetMaterialProperties() const
{
	return static_cast<void*>(Mapped + Slot * MaterialSize);
}

void Material::Render()
{
	// Commit material changes to gpu
	Materials->Unmap();

	// Fill-Stage
	{
		Camera::Begin();
		Framebuffer->Use();

		// Clear framebuffer, but keep the framebuffer since it's shared
		Camera::Clear(Vector3(), CLEAR_BACKGROUND);

		// Bind the fill effect
		Deffered::Fill->Use();

		for (auto& Material : Batch)
		{
			Mesh* mesh = Material->mesh;
			mesh->Use();

			if (Material->texture != nullptr)
			{
				Material->texture->Use();
			}

			Matrix m = Material->GetParent()->GetWorldMatrix();
			Matrix mvp = Camera::GetProjection() * Camera::GetView() * m;

			Deffered::Fill->Set("mid", Material->Slot);
			Deffered::Fill->Set("mvp", mvp);
			Deffered::Fill->Set("m", m);

			if (mesh->HasBuffer(BUFFER_TARGET_INDICES))
			{
				Buffer* Indices = mesh->GetBuffer(BUFFER_TARGET_INDICES);
				Camera::Draw(DRAW_MODE_TRIANGLES, true, Indices->GetSize() / sizeof(int));
			}
			else
			{
				Buffer* Vertices = mesh->GetBuffer(BUFFER_TARGET_VERTICES);
				Camera::Draw(DRAW_MODE_TRIANGLES, false, Vertices->GetSize() / sizeof(Vector3));
			}
		}

		Camera::End();
	}
	
	// Deffered-Stage
	{
		Camera::Begin();
		Camera::Clear(Vector3(), CLEAR_BOTH);

		Deffered::Quad->Use();

		// Prepare the effect
		Effect->Use();
		Effect->Set("dl_position", Vector3(0, 0, 0));
		Effect->Set("dl_direction", Vector3(-1, 0, 0));
		Effect->Set("camera_position", Camera::position);
		Effect->Set("ambient", Deffered::Background * 0.66f);

		// Bind the material buffer
		Effect->SetShaderBuffer(*Materials, 0);

		// Bind the different render textures
		Identities->Use(Effect->GetSampler("materials"));
		Positions->Use(Effect->GetSampler("positions"));
		Normals->Use(Effect->GetSampler("normals"));
		Colors->Use(Effect->GetSampler("colors"));

		Camera::Draw(DRAW_MODE_TRIANGLES, false, 6);
		Camera::End();
	}

	Mapped = static_cast<uint8_t*>(Materials->Map(BUFFER_ACCESS_BOTH));
}

Material::Material(Actor* parent) : Component(parent)
{
	if (!Slots.empty())
	{
		// Take the next free slot
		Slot = Slots.front();
		Slots.pop();

		Batch.push_back(this);
	}
	else if (Count < Max)
	{
		Slot = Count++;
		Batch.push_back(this);
	}
	else
	{
		PERROR("ERROR_BATCH_FULL");
		throw;
	}
}

void Material::SetTexture(Texture* texture)
{
  this->texture = texture;
}

void Material::SetMesh(Mesh* mesh)
{
	this->mesh = mesh;
}

Mesh* Material::GetMesh() const
{
	return mesh;
}

Texture* Material::GetTexture() const
{
  return texture;
}

void Material::OnDestroy()
{
	Batch.erase(std::find(Batch.begin(), Batch.end(), this));
}