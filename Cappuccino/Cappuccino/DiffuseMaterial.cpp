#include "DiffuseMaterial.h"

#include <Asset.h>
#include <Actor.h>
#include <Camera.h>
#include <Log.h>
#include <Mesh.h>

using namespace Core;

struct DiffuseMaterialProperties
{
	Vector3 Diffuse;
};

std::unique_ptr<Effect> DiffuseMaterial::Effect;

DiffuseMaterial::DiffuseMaterial(Actor* Parent) : Material(Parent)
{
	DiffuseMaterialProperties& Properties = GetMaterialProperties<DiffuseMaterialProperties>();
	Properties.Diffuse = Vector3(0.8f);
}

void DiffuseMaterial::SetDiffuseColor(Vector3 Diffuse)
{
	DiffuseMaterialProperties& Properties = GetMaterialProperties<DiffuseMaterialProperties>();
	Properties.Diffuse = Diffuse;
}

Vector3 DiffuseMaterial::GetDiffuseColor() const
{
	DiffuseMaterialProperties& Properties = GetMaterialProperties<DiffuseMaterialProperties>();
	return Properties.Diffuse;
}

void DiffuseMaterial::OnTick()
{
  /*if (mesh != nullptr && GetEnabled(true))
  {
    mesh->Use();
    effect->Use();

    if (texture != nullptr)
    {
      texture->Use();
    }
    
    Matrix m = GetParent()->GetWorldMatrix();
    Matrix mvp = Camera::GetProjection() * Camera::GetView() * m;

		effect->Set("id", effect->GetIdentity());
    effect->Set("mvp", mvp);
    effect->Set("m", m);
    effect->Set("diffuse", diffuse);

    if (mesh->HasBuffer(BUFFER_TARGET_INDICES))
    {
			Buffer* buffer = mesh->GetBuffer(BUFFER_TARGET_INDICES);
      Camera::Draw(DRAW_MODE_TRIANGLES, true, buffer->GetSize() / sizeof(int));
    }
    else
    {
			Buffer* buffer = mesh->GetBuffer(BUFFER_TARGET_VERTICES);
      Camera::Draw(DRAW_MODE_TRIANGLES, false, buffer->GetSize() / sizeof(Vector3));
    }
  }*/
}

Type DiffuseMaterial::GetType()
{
  return typeid(DiffuseMaterial);
}

void DiffuseMaterial::Initialize()
{
	DiffuseMaterial::Initialize<DiffuseMaterialProperties>(1000);

  Shader* Vs = new Shader(SHADER_TYPE_VERTEX, Asset("Shaders/Deffered/Diffuse/Shader.vs").GetText());

  if (!Vs->IsValid())
  {
    PERROR("ERROR_DEFFERED_DIFFUSE_VERTEX_SHADER: ", Vs->GetLog().c_str());
    throw;
  }

  Shader* Fs = new Shader(SHADER_TYPE_FRAGMENT, Asset("Shaders/Deffered/Diffuse/Shader.fs").GetText());

  if (!Fs->IsValid())
  {
    PERROR("ERROR_DEFFERED_DIFFUSE_FRAGMENT_SHADER: ", Fs->GetLog().c_str());
    throw;
  }

	Effect = std::unique_ptr<Core::Effect>(new Core::Effect({ Vs, Fs }, 2));

  if (!Effect->IsValid())
  {
    PERROR("ERROR_DEFFERED_DIFFUSE_EFFECT: ", Effect->GetLog().c_str());
    throw;
  }

  SUCCESS("Deffered/Diffuse: OK");
}
