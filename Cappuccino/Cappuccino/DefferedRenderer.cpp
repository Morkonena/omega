#include "DefferedRenderer.h"

#include <Asset.h>
#include <Camera.h>
#include <Mesh.h>
#include <Log.h>
#include <Pool.h>

using namespace Core;

#pragma region Static

int Deffered::Width;
int Deffered::Height;

Vector3 Deffered::Background;

std::unique_ptr<Framebuffer> Deffered::Framebuffer;
std::unique_ptr<Texture> Deffered::DepthStencilBuffer;

std::unique_ptr<Mesh> Deffered::Quad;

std::unique_ptr<Effect> Deffered::Combine;
std::unique_ptr<Effect> Deffered::Fill;

std::vector<void(*)()> Deffered::Materials;

#pragma endregion

void Deffered::CreateQuad()
{
	Vector3 Vertices[] =
	{
		Vector3(-1,  1,  0),
		Vector3(1,  1,  0),
		Vector3(-1, -1,  0),

		Vector3(-1, -1,  0),
		Vector3(1,  1,  0),
		Vector3(1, -1,  0)
	};

	Vector2 UVs[] =
	{
		Vector2(0, 1),
		Vector2(1, 1),
		Vector2(0, 0),

		Vector2(0, 0),
		Vector2(1, 1),
		Vector2(1, 0)
	};

	Quad = std::make_unique<Mesh>();
	Quad->CreateBuffer(BUFFER_TARGET_VERTICES).SetData(Vertices, 6);
	Quad->CreateBuffer(BUFFER_TARGET_UVS, BUFFER_TARGET_INPUT_VECTOR2).SetData(UVs, 6);

	if (!Quad->Upload())
	{
		PERROR("ERROR_CREATE_DEFFERED_QUAD");
		throw;
	}
}

void Deffered::CreateCombineEffect()
{
	Shader* Vs = new Shader(SHADER_TYPE_VERTEX, Asset("Shaders/Deffered/Combine/Shader.vs").GetText());

	if (!Vs->IsValid())
	{
		PERROR("ERROR_DEFFERED_COMBINE_VERTEX_SHADER: ", Vs->GetLog().c_str());
		throw;
	}

	Shader* Fs = new Shader(SHADER_TYPE_FRAGMENT, Asset("Shaders/Deffered/Combine/Shader.fs").GetText());

	if (!Fs->IsValid())
	{
		PERROR("ERROR_DEFFERED_COMBINE_FRAGMENT_SHADER: ", Fs->GetLog().c_str());
		throw;
	}

	Combine = std::make_unique<Effect>(std::initializer_list<Shader*>({ Vs, Fs }), 2);

	if (!Combine->IsValid())
	{
		PERROR("ERROR_DEFFERED_COMBINE_EFFECT: ", Combine->GetLog().c_str());
		throw;
	}

	Combine->AddSampler("layers");
}

void Deffered::CreateFillEffect()
{
	Shader* Vs = new Shader(SHADER_TYPE_VERTEX, Asset("Shaders/Deffered/Fill/Shader.vs").GetText());

	if (!Vs->IsValid())
	{
		PERROR("ERROR_DEFFERED_FILL_VERTEX_SHADER: ", Vs->GetLog().c_str());
		throw;
	}

	Shader* Fs = new Shader(SHADER_TYPE_FRAGMENT, Asset("Shaders/Deffered/Fill/Shader.fs").GetText());

	if (!Fs->IsValid())
	{
		PERROR("ERROR_DEFFERED_FILL_FRAGMENT_SHADER: ", Fs->GetLog().c_str());
		throw;
	}

	Fill = std::make_unique<Effect>(std::initializer_list<Shader*>({ Vs, Fs }), 2);

	if (!Fill->IsValid())
	{
		PERROR("ERROR_DEFFERED_FILL_EFFECT: ", Fill->GetLog().c_str());
		throw;
	}

	Fill->AddSampler("sampler");
}

void Deffered::Initialize(const Window& window)
{
	Width = window.GetWidth();
	Height = window.GetHeight();

	Framebuffer = std::make_unique<Core::Framebuffer>();

	DepthStencilBuffer = std::make_unique<Texture>();
	DepthStencilBuffer->SetPixels(TEXTURE_FORMAT_D24S8, Width, Height, nullptr);
	DepthStencilBuffer->SetSmoothing(false);

	Framebuffer->Attach(DepthStencilBuffer.get(), FRAMEBUFFER_OUTPUT_DEPTH);

	if (!Framebuffer->IsValid())
	{
		Framebuffer->Log();

		PERROR("ERROR_CREATE_DEFFERED_FRAMEBUFFER");
		throw;
	}

	CreateQuad();
	CreateCombineEffect();
	CreateFillEffect();

	SUCCESS("Deffered: OK");
}

void Deffered::SetBackgroundColor(Vector3 background)
{
	Background = background;
}

void Deffered::Render()
{
	Camera::Begin();
	Framebuffer->Use();

	Camera::Clear(Vector3(), CLEAR_BOTH);

	for (void(*x)() : Materials)
	{
		x(); // TODO: Multithreading
	}

	Camera::End();
}

Effect* Deffered::GetFillEffect()
{
	return Fill.get();
}
