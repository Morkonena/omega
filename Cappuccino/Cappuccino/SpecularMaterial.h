//
// Created by M�rk�nen� on 23.7.2018
//

#ifndef CORE_SPECULAR_MATERIAL_H
#define CORE_SPECULAR_MATERIAL_H

#include "Material.h"

namespace Core
{
  class SpecularMaterial : public Material
  {
  public:
		SpecularMaterial(Actor* Parent);
	
		void SetDiffuseColor  (Vector3 Diffuse);
		void SetSpecularColor (Vector3 Specular);
		void SetPower				  (float   Power);

		Vector3 GetDiffuseColor()  const;
		Vector3 GetSpecularColor() const;
		float		GetPower()				 const;

    virtual void OnTick() override;
    virtual Type GetType() override;

    static void Initialize();
  };
}

#endif // !CORE_SPECULAR_RENDERER_H