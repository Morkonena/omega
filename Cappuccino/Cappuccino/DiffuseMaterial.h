//
// Created by M�rk�nen� on 23.7.2018
//

#ifndef CORE_DIFFUSE_MATERIAL_H
#define CORE_DIFFUSE_MATERIAL_H

#include "Material.h"

namespace Core
{
  class DiffuseMaterial : public Material
  {
  public:
		DiffuseMaterial(Actor* Parent);

		void			SetDiffuseColor(Vector3 color);
		Vector3		GetDiffuseColor() const;

    virtual void OnTick() override;
    virtual Type GetType() override;

    static void Initialize();
  };
}

#endif // !CORE_DIFFUSE_MATERIAL_H