#include "Asset.h"
#include "Console.h"
#include "Log.h"

using namespace Core;

#ifdef ANDROID

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

void Assets::Initialize(JNIEnv *env, jobject* manager)
{
  // Create the asset manager from a java object
  ::manager = AAssetManager_fromJava(env, *manager);
  PRINT("Assets: OK");
}

Asset::Asset(const char *filename)
{
  ASSERT(filename != 0);

  // Open the asset
  AAsset *handle = AAssetManager_open(manager, filename, AASSET_MODE_BUFFER);

  // Make sure the asset is open
  if (handle == 0)
  {
    FERROR("ERROR_LOAD_ASSET (%s)", filename);
    throw;
  }

  // Allocate a buffer where the asset can be loaded
  length = (int)AAsset_getLength(handle);
  memory = new char[length + 1];
  memory[length] = 0;

  // Copy the asset into the buffer and then close the asset handle
  AAsset_read(handle, (void*)memory, (size_t)length);
  AAsset_close(handle);
}

#else

#include <Windows.h>
#include <fstream>

std::string Assets::folder;

void Assets::Initialize()
{
  // Get this program's filename 
  CHAR filename[200];
  GetModuleFileName(0, filename, 200);

  // Find the last separator and cut of the filename
  folder = std::string(filename);
  folder = folder.substr(0, folder.find_last_of("\\")) + "\\";

  SUCCESS("Assets: OK");
}

std::string Assets::GetFolder()
{
  return folder;
}

Asset::Asset(std::string filename)
{
  ASSERT(!filename.empty());

  // Open the asset and start from the end
  std::ifstream stream(Assets::GetFolder() + filename, std::ios::in | std::ios::binary | std::ios::ate);

  if (!stream.is_open())
  {
    PERROR("ERROR_LOAD_ASSET (", filename.c_str(), ")");
    throw;
  }

  length = (int)stream.tellg();

  // Seek to the start
  stream.seekg(0, std::ios::beg);

  // Allocate a buffer for storing the asset
  memory.resize(length + 1);
  memory[length] = 0;

  // Copy the asset to the buffer
  stream.read(memory.data(), length);

  // Close the asset
  stream.close();
}

#endif

std::string Asset::GetText() const
{
  return std::string(memory.data());
}

void* Asset::GetData() const
{
  return memory.data();
}

int Asset::GetLength() const
{
  return length;
}


