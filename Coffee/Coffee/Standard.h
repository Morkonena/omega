//
// Created by M�rk�nen� on 14.7.2018
//

#ifndef CORE_STANDARD_H
#define CORE_STANDARD_H

#include <assert.h>
#include <algorithm>
#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <vector>
#include <memory>

#define ASSERT(X) assert(X)

#endif // !CORE_STANDARD_H