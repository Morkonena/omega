//
// Created by M�rk�nen� on 3.9.2018
//

#ifndef CORE_TEXTURE_ARRAY_H
#define CORE_TEXTURE_ARRAY_H

#include "Texture.h"

namespace Core
{
  class TextureArray
  {
  private:
    std::uint32_t id;

    TextureFormat format;

    int width;
    int height;
    int length;

  public:
    TextureArray(TextureFormat format, int width, int height, int length);
    TextureArray(TextureArray&& texture_array);
    TextureArray& operator=(TextureArray&& texture_array);
    ~TextureArray();

    void Set(int i, const Texture& texture);
    void Use(Sampler sampler = 0);

    TextureFormat GetFormat() const;

    int GetWidth() const;
    int GetHeight() const;

    int GetLength() const;
  };
}

#endif // !CORE_TEXTURE_ARRAY