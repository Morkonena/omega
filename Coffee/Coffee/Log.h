//
// Created by M�rk�nen� on 10.7.2018
//

#ifndef CORE_LOG_H
#define CORE_LOG_H

#ifdef ANDROID

#include <android/log.h>

#define PRINT(X) __android_log_write(ANDROID_LOG_INFO, "Coffee", X)
#define PRINTF(X, ...) __android_log_print(ANDROID_LOG_INFO, "Coffee", X, __VA_ARGS__)

#define _PERROR_(X) __android_log_write(ANDROID_LOG_ERROR, "Coffee", X)
#define _FERROR_(X, ...) __android_log_print(ANDROID_LOG_ERROR, "Coffee", X, __VA_ARGS__)

#define PERROR(X) _PERROR_(""); \
                  _PERROR_(__FUNCTION__); \
				  _PERROR_(X); \
				  _PERROR_("")

#define FERROR(X, ...)  _PERROR_(""); \
                        _FERROR_("(%s)", __FUNCTION__); \
                        _FERROR_(X, __VA_ARGS__); \
                        _PERROR_("")                  
#else

#include <stdio.h>
#include "Console.h"

#define PRINT(...) Core::Console::WriteLine(__VA_ARGS__);

#define WARNING(...) Core::Console::SetFlags(Core::CONSOLE_TEXT_RED | Core::CONSOLE_TEXT_GREEN | Core::CONSOLE_TEXT_STRONG); \
                     Core::Console::WriteLine(__VA_ARGS__); \
                     Core::Console::ResetFlags();

#define SUCCESS(...) Core::Console::SetFlags(Core::CONSOLE_TEXT_GREEN | Core::CONSOLE_TEXT_STRONG); \
                     Core::Console::WriteLine(__VA_ARGS__); \
                     Core::Console::ResetFlags();

#define PERROR(...) Core::Console::SetFlags(Core::CONSOLE_TEXT_RED | Core::CONSOLE_TEXT_STRONG); \
                    Core::Console::WriteLine(); \
                    Core::Console::WriteLine("(", __FUNCTION__, ")"); \
                    Core::Console::WriteLine(__VA_ARGS__); \
                    Core::Console::WriteLine(); \
                    Core::Console::ResetFlags();
#endif

#endif // !CORE_LOG_H