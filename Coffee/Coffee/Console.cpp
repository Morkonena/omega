#include "Console.h"
using namespace Core;

#include <Windows.h>

HANDLE console;

void Console::Initialize()
{
  console = GetStdHandle(STD_OUTPUT_HANDLE);
}

void Console::SetFlags(int flags)
{
  SetConsoleTextAttribute(console, flags);
}

void Console::ResetFlags()
{
  SetConsoleTextAttribute(console, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}
