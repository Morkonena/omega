//
// Created by M�rk�nen� on 16.10.2018
//

#ifndef CORE_CONSOLE_H
#define CORE_CONSOLE_H

#include <iostream>

namespace Core
{
  enum TextColorFlags : int
  {
    CONSOLE_TEXT_BLUE = 1,
    CONSOLE_TEXT_GREEN = 2,
    CONSOLE_TEXT_RED = 4,
    CONSOLE_TEXT_STRONG = 8
  };

  enum BackgroundColorFlags : int
  {
    CONSOLE_BACKGROUND_BLUE = 16,
    CONSOLE_BACKGROUND_GREEN = 32,
    CONSOLE_BACKGROUND_RED = 64,
    CONSOLE_BACKGROUND_STRONG = 128
  };

  class Console
  {
  private:
    template<typename A, typename... B>
    static void Continue(A& a, B&... list);

    static inline void Continue() {};

  public:
    static void Initialize();

    static void SetFlags(int flags);
    static void ResetFlags();

    template<typename... T>
    static void Write(T&&... list);

    template<typename... T>
    static void WriteLine(T&&... list);
  };

  template<typename A, typename ...B>
  inline void Console::Continue(A& a, B&... list)
  {
    std::cout << a;
    Continue(list...);
  }

  template<typename... T>
  inline void Console::Write(T&&... list)
  {
    Continue(list...);
  }

  template<typename ...T>
  inline void Console::WriteLine(T&&... list)
  {
    Continue(list...);
    std::cout << std::endl;
  }
}

#endif // !CORE_CONSOLE_H