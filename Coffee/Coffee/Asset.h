//
// Created by M�rk�nen� on 10.7.2018
//

#ifndef CORE_ASSET_H
#define CORE_ASSET_H

#include "Standard.h"

#ifdef ANDROID
#include <jni.h>
#endif

namespace Core
{
  class Asset
  {
  private:
    mutable std::vector<char> memory;
    int length;

  public:
    Asset(std::string filename);

    std::string GetText() const;

    void* GetData() const;
    int GetLength() const;
  };

#ifndef ANDROID

  class Assets
  {
  private:
    static std::string folder;

  public:
    static void Initialize();
    static std::string GetFolder();
  };

#else

  typedef struct AAssetManager* AssetManager;

  class Assets
  {
  private:
    AssetManager manager;
  public:
    static void Initialize(JNIEnv* environment, jobject asset_manager);
  };

#endif
}

#endif // !CORE_ASSET_H