#include "GL.h"
#include "TextureArray.h"

using namespace Core;

constexpr int storage_formats[] =
{
  GL_R8,
  GL_RG8,
  GL_RGB8,
  GL_RGBA8
};

constexpr int internal_formats[] =
{
  GL_RED,
  GL_RG,
  GL_RGB,
  GL_RGBA
};

TextureArray::TextureArray(TextureFormat format, int width, int height, int length) :
  format(format), width(width), height(height), length(length)
{
  // Create a texture array
  glGenTextures(1, &id);
  glBindTexture(GL_TEXTURE_2D_ARRAY, id);

  // Configure the array
  glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, storage_formats[format], width, height, length);

  // TODO: CONFIGURABLE
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

Core::TextureArray::TextureArray(TextureArray && texture_array)
{
}

TextureArray& TextureArray::operator=(TextureArray&& texture_array)
{
  format = texture_array.format;
  height = texture_array.height;
  id = texture_array.id;
  length = texture_array.length;
  width = texture_array.width;

  texture_array.id = 0;

  return *this;
}

TextureArray::~TextureArray()
{
  if (id > 0)
  {
    glDeleteTextures(1, &id);
  }
}

void TextureArray::Set(int i, const Texture& texture)
{
  // Texture must fit to the array and be the same format
  ASSERT(texture.GetWidth() <= width && texture.GetHeight() <= height && texture.GetFormat() == format);

  glBindTexture(GL_TEXTURE_2D_ARRAY, id);
  glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, width, height, 1, internal_formats[format], GL_UNSIGNED_BYTE, texture.GetPixels());
}

void TextureArray::Use(Sampler sampler)
{
  glActiveTexture(GL_TEXTURE0 + sampler);
  glBindTexture(GL_TEXTURE_2D_ARRAY, id);
}

TextureFormat TextureArray::GetFormat() const
{
  return format;
}

int TextureArray::GetWidth() const
{
  return width;
}

int TextureArray::GetHeight() const
{
  return height;
}

int TextureArray::GetLength() const
{
  return length;
}
