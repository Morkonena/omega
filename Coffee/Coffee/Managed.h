//
// Created by M�rk�nen� on 14.7.2018
//

#ifndef CORE_MANAGED_H
#define CORE_MANAGED_H

#include <list>

namespace Core
{
  template<class T>
  class Managed
  {
  private:
    mutable T* memory;
    mutable std::list<const Managed<T>*> connections;

    void Setup(const Managed<T>& managed) const;

    void Connect(const Managed<T>* managed) const;
    void Disconnect(const Managed<T>* managed) const;

  public:
    Managed<T>(T* x);
    Managed<T>(const Managed<T>& x);

    Managed<T>& operator=(const Managed<T>& managed);

    T* operator->();
    const T* operator->() const;

    T* GetData();
    const T* GetData() const;

    void Stop();
    void Reset();

    ~Managed<T>();
  };

  template<class T>
  inline Managed<T>::Managed(T* x)
  {
    memory = x;
  }

  template<class T>
  inline Managed<T>::Managed(const Managed<T>& x)
  {
    Setup(x);
  }

  template<class T>
  inline void Managed<T>::Setup(const Managed<T>& x) const
  {
    memory = x.memory;

    Connect(&x);
    x.Connect(this);
  }

  template<class T>
  inline void Managed<T>::Connect(const Managed<T>* x) const
  {
    connections.push_back(x);
  }

  template<class T>
  inline void Managed<T>::Disconnect(const Managed<T>* x) const
  {
    connections.remove(x);
  }

  template<class T>
  inline Managed<T>& Managed<T>::operator=(const Managed<T>& x)
  {
    Reset();
    Setup(x);

    return *this;
  }

  template<class T>
  inline T* Managed<T>::operator->()
  {
    return memory;
  }

  template<class T>
  inline const T* Managed<T>::operator->() const
  {
    return memory;
  }

  template<class T>
  inline T* Managed<T>::GetData()
  {
    return memory;
  }

  template<class T>
  inline const T* Managed<T>::GetData() const
  {
    return memory;
  }

  template<class T>
  inline void Managed<T>::Stop()
  {
    memory = nullptr;
    connections.clear();
  }

  template<class T>
  inline void Managed<T>::Reset()
  {
    if (connections.empty())
    {
      if (memory != nullptr)
      {
        delete(memory);
        memory = nullptr;
      }
    }
    else
    {
      for (const Managed<T>* connection : connections)
      {
        connection->Disconnect(this);
      }
    }
  }

  template<class T>
  inline Managed<T>::~Managed()
  {
    Reset();
  }
}

#endif // !CORE_MANAGED_H