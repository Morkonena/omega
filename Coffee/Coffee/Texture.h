//
// Created by M�rk�nen� on 17.7.2018
//

#ifndef CORE_TEXTURE_H
#define CORE_TEXTURE_H

#include "Effect.h"

namespace Core
{
  enum TextureFormat
  {
    TEXTURE_FORMAT_RGB8,
    TEXTURE_FORMAT_RGB32,
    TEXTURE_FORMAT_RGBA8,
    TEXTURE_FORMAT_RGBA32,
    TEXTURE_FORMAT_D24S8,
		TEXTURE_FORMAT_FLOAT,
		TEXTURE_FORMAT_INT,
		TEXTURE_FORMAT_UINT
  };

  class Texture
  {
  private:
    std::uint32_t id;

    TextureFormat format;

    int width;
    int height;

    void* pixels;

  public:
    Texture();
    Texture(Texture&& texture);
    Texture& operator=(Texture&& texture);
    ~Texture();

    bool IsValid();

    void Use(Sampler sampler = 0);

    void SetPixels(TextureFormat format, int width, int height, void* pixels);
    void SetSmoothing(bool enable);

    std::uint32_t GetIdentity() const;

    TextureFormat GetFormat() const;

    int GetWidth() const;
    int GetHeight() const;

    void* GetPixels() const;
  };
}

#endif // !CORE_TEXTURE_H