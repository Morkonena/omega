//
// Created by M�rk�nen� on 9.7.2018
//

#ifndef CORE_MESH_H
#define CORE_MESH_H

#include "Camera.h"
#include "Buffer.h"
#include "Vector2.h"
#include "Vector3.h"

namespace Core
{
  enum BufferTarget : int
  {
    BUFFER_TARGET_VERTICES,
    BUFFER_TARGET_UVS,
    BUFFER_TARGET_NORMALS,
		BUFFER_TARGET_COLORS,
    BUFFER_TARGET_CUSTOM,
    BUFFER_TARGET_INDICES
  };

  enum BufferTargetInput : int
  {
    BUFFER_TARGET_INPUT_FLOAT = 1,
    BUFFER_TARGET_INPUT_VECTOR2 = 2,
    BUFFER_TARGET_INPUT_VECTOR3 = 3,
    BUFFER_TARGET_INPUT_VECTOR4 = 4
  }; 

  class Mesh
  {
  private:
    class MeshBuffer : public Buffer
    {    
    private:
      BufferTargetInput input;
      BufferTarget target;

    public:
      using Buffer::Buffer;

      void SetTarget(BufferTarget target);
      void SetTargetInput(BufferTargetInput input);

      BufferTarget GetTarget() const;
      BufferTargetInput GetTargetInput() const;
    };

  private:
    std::uint32_t id;  
    mutable std::vector<std::unique_ptr<MeshBuffer>> buffers;

  public:
    Mesh() = default;
    Mesh(Mesh&& mesh) = default;
    Mesh& operator=(Mesh&& mesh) = default;
    ~Mesh();

    Buffer& CreateBuffer(BufferTarget target, BufferTargetInput input = BUFFER_TARGET_INPUT_VECTOR3, BufferUsage usage = BUFFER_USAGE_STATIC);
    
    Buffer* GetBuffer(BufferTarget target) const;
    Buffer& GetBufferAt(int i) const;

		bool HasBuffer(BufferTarget target) const;
    
    int GetBufferCount();

    bool Upload();
    void Use() const;
  };
}

#endif // !CORE_MESH_H