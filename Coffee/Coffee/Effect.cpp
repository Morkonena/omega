#include "Effect.h"
#include "GL.h"
#include "Log.h"

using namespace Core;

Effect::Effect() {}

Effect::Effect(std::initializer_list<Shader*> shaders, int count)
{
  // Create a empty effect
  id = glCreateProgram();

  // Attach all passed shaders to this effect
  for (Shader* shader : shaders)
  {
    glAttachShader(id, shader->GetIdentity());
  }

  // Compile this effect
  glLinkProgram(id);
}

bool Effect::IsValid() const
{
  if (id == 0)
  {
    return false;
  }

  int i;
  glGetProgramiv(id, GL_LINK_STATUS, &i);

  return i == GL_TRUE;
}

std::string Effect::GetLog() const
{
  int length;
  glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);

  std::vector<char> log(length + 1);
  log[length] = 0;

  glGetProgramInfoLog(id, length, &length, log.data());

  return std::string(log.data());
}

std::uint32_t Effect::GetIdentity() const
{
	return id;
}

void Effect::Use()
{
  glUseProgram(id);
}

void Effect::Set(const char* name, int number)
{
  glUniform1i(glGetUniformLocation(id, name), number);
}

void Effect::Set(const char* name, unsigned int number)
{
	glUniform1ui(glGetUniformLocation(id, name), number);
}

void Effect::Set(const char* name, float number)
{
  glUniform1f(glGetUniformLocation(id, name), number);
}

void Effect::Set(const char* name, const Vector3& vector)
{
	
  glUniform3f(glGetUniformLocation(id, name), vector.x, vector.y, vector.z);
}

void Effect::Set(const char* name, const Matrix& matrix)
{
  glUniformMatrix4fv(glGetUniformLocation(id, name), 1, GL_FALSE, (float*)&matrix);
}

void Effect::AddSampler(const char* name)
{
  glUseProgram(id);

  int location = glGetUniformLocation(id, name);

  if (location != -1)
  {
    int id = (int)samplers.size();
    glUniform1i(location, id);

    samplers.insert(std::make_pair(std::string(name), id));
  }
}

Sampler Effect::GetSampler(const char* name) const
{
	auto i = samplers.find(std::string(name));
	return (i == samplers.end()) ? -1 : i->second;
}

void Effect::SetShaderBuffer(Buffer& buffer, int binding)
{
	if (buffer.GetType() != BUFFER_TYPE_SHADER)
	{
		PERROR("ERROR_INVALID_BUFFER");
		throw;
	}

	buffer.Use();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, buffer.GetIdentity());
}

