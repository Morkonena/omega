//
// Created by M�rk�nen� on 10.7.2018
//

#ifndef CORE_SHADER_H
#define CORE_SHADER_H

#include "Standard.h"

namespace Core
{
  enum ShaderType : int
  {
    SHADER_TYPE_VERTEX,
    SHADER_TYPE_FRAGMENT
  };

  class Shader
  {
  private:
    std::uint32_t id;

  public:
    Shader(ShaderType type, std::string text);
    ~Shader();

    bool IsValid() const;

    std::string GetLog() const;
    int GetIdentity() const;
  };
}

#endif // !CORE_SHADER_H