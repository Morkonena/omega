//
// Created by M�rk�nen� on 10.7.2018
//

#ifndef CORE_EFFECT_H
#define CORE_EFFECT_H

#include "Buffer.h"
#include "Matrix.h"
#include "Shader.h"
#include "Standard.h"
#include "Vector3.h"

namespace Core
{
  typedef std::int32_t Sampler;

  class Effect
  {
  private:
    std::uint32_t id;
    std::map<std::string, Sampler> samplers;

  public:
		Effect();
    Effect(std::initializer_list<Shader*> shaders, int count);

    bool IsValid() const;

    std::string GetLog() const;
		std::uint32_t GetIdentity() const;

    void Use();
    void Set(const char* name, int number);
		void Set(const char* name, unsigned int number);
    void Set(const char* name, float number);
    void Set(const char* name, const Vector3& vector);
    void Set(const char* name, const Matrix& matrix);

    void AddSampler(const char* name);
    Sampler GetSampler(const char* name) const;

		void SetShaderBuffer(Buffer& buffer, int binding);
  };
}

#endif // !CORE_EFFECT_H