#include "Mesh.h"
#include "GL.h"
#include "Log.h"

using namespace Core;

bool Mesh::Upload()
{
  glGenVertexArrays(1, &id);
  glBindVertexArray(id);

  for (auto& i : buffers)
  {
    MeshBuffer& buffer = *i;
    buffer.Use();

    if (buffer.GetTarget() == BUFFER_TARGET_INDICES)
    {
      continue;
    }
		
    glVertexAttribPointer(buffer.GetTarget(), buffer.GetTargetInput(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(buffer.GetTarget());
		
		/*if (buffer.GetTarget() == BUFFER_TARGET_NORMALS)
		{
			glVertexAttribDivisor(buffer.GetTarget(), 3);
		}*/
  }

  glBindVertexArray(0);
  return true;
}

void Mesh::Use() const
{
  glBindVertexArray(id);
}

Mesh::~Mesh()
{
  glDeleteVertexArrays(1, &id);
}

Buffer& Mesh::CreateBuffer(BufferTarget target, BufferTargetInput input, BufferUsage usage)
{
  BufferType type = (target == BUFFER_TARGET_INDICES) ? BUFFER_TYPE_INDEX : BUFFER_TYPE_DATA;

  std::unique_ptr<MeshBuffer> buffer = std::make_unique<MeshBuffer>(type, usage);
  buffer->SetTarget(target);
  buffer->SetTargetInput(input);

  MeshBuffer* address = buffer.get();
  buffers.push_back(std::move(buffer));

  return *address;
}

Buffer* Mesh::GetBuffer(BufferTarget target) const
{
  auto i = std::find_if(buffers.begin(), buffers.end(), [target](std::unique_ptr<MeshBuffer>& x) { return x->GetTarget() == target; });
  return (i == buffers.end()) ? nullptr : i->get();
}

Buffer& Mesh::GetBufferAt(int i) const
{
  return *buffers[i];
}

bool Mesh::HasBuffer(BufferTarget target) const
{
	return std::find_if(buffers.begin(), buffers.end(), [target](std::unique_ptr<MeshBuffer>& x) { return x->GetTarget() == target; }) != buffers.end();
}

int Mesh::GetBufferCount()
{
  return (int)buffers.size();
}

void Mesh::MeshBuffer::SetTarget(BufferTarget target)
{
  this->target = target;
}

void Mesh::MeshBuffer::SetTargetInput(BufferTargetInput input)
{
  this->input = input;
}

BufferTarget Mesh::MeshBuffer::GetTarget() const
{
  return target;
}

BufferTargetInput Mesh::MeshBuffer::GetTargetInput() const
{
  return input;
}
