#include "PhysX.h"
#include "Static.h"

#include <Actor.h>

using namespace Core;

Static::Static(Actor* parent) : Component(parent)
{
  if (GetParent()->HasComponent<Shape>())
  {
    Shape* shape = GetParent()->GetComponent<Shape>();

    body = Physics::CreateStatic(GetParent()->GetWorldPosition(), GetParent()->GetWorldRotation(), *shape);
    body->userData = GetParent();

    Physics::Add(body);
  }
}

void Static::OnPhysicsTick()
{
  Vector3 position = GetParent()->GetWorldPosition();
  Vector3 rotation = GetParent()->GetWorldRotation();

  PxVec3 transform_position = *reinterpret_cast<PxVec3*>(&position);
  PxQuat transform_rotation = PxQuat(rotation.x, PxVec3(1, 0, 0)) * PxQuat(rotation.y, PxVec3(0, 1, 0)) * PxQuat(rotation.z, PxVec3(0, 0, 1));

  PxTransform transform(transform_position, transform_rotation);
  body->setGlobalPose(transform);
}

void Static::OnDestroy()
{
  Physics::Remove(body);
}

Type Static::GetType()
{
  return typeid(Static);
}
