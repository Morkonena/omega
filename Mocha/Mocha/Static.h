//
// Created by M�rk�nen� on 26.10.2018
//

#ifndef CORE_STATIC_H
#define CORE_STATIC_H

#include <Component.h>
#include "Physics.h"

namespace Core
{
  class Static : public Component
  {
  private:
    PhysX::PxRigidStatic* body;

  public:
    Static(Actor* parent);

    virtual void OnPhysicsTick() override;

    virtual void OnDestroy() override;
    virtual Type GetType() override;
  };
}

#endif // !CORE_STATIC_H