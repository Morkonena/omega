//
// Created by M�rk�nen� on 24.10.2018
//

#ifndef CORE_PHYSICS_H
#define CORE_PHYSICS_H

#include <Component.h>
#include <Effect.h>
#include <Vector3.h>

#include <memory>

namespace physx
{
  class PxDefaultAllocator;
  class PxDefaultErrorCallback;

  class PxFoundation;
  class PxPhysics;
  class PxCooking;

  class PxRigidActor;

  class PxRigidBody;
  class PxRigidDynamic;
  class PxRigidStatic;

  class PxGeometry;
  class PxMaterial;

  class PxScene;

  class PxDefaultCpuDispatcher;
  class PxDefaultGpuDispatcher;
}

namespace PhysX = physx;

namespace Core
{
  class Shape : public Component
  {
  public:
    inline Shape (Actor* actor) : Component(actor) {}

    Vector3 position;
    Vector3 rotation;

    float density;

    virtual inline Type GetType() override
    {
      return typeid(Shape);
    }

  private:
    friend class Physics;
    virtual std::unique_ptr<PhysX::PxGeometry> GetGeometry() = 0;
  };

  class Box : public Shape
  {
  public:
    Vector3 extent;
    using Shape::Shape;

  private:
    virtual std::unique_ptr<PhysX::PxGeometry> GetGeometry() override;
  };

  class Sphere : public Shape
  {
  public:
    float radius;
    using Shape::Shape;

  private:
    virtual std::unique_ptr<PhysX::PxGeometry> GetGeometry() override;
  };

  class Capsule : public Shape
  {
  public:
    float radius;
    float height;
    using Shape::Shape;

  private:
    virtual std::unique_ptr<PhysX::PxGeometry> GetGeometry() override;
  };

  class Custom : public Shape
  {
  public:
    Vector3* triangles;
    int count;
    using Shape::Shape;

  private:
    virtual std::unique_ptr<PhysX::PxGeometry> GetGeometry() override;
  };

  class Physics
  {
  private:
    static PhysX::PxDefaultAllocator*       allocator;
    static PhysX::PxDefaultErrorCallback*   errors;

    static PhysX::PxFoundation* foundation;
    static PhysX::PxPhysics*    physics;
    static PhysX::PxCooking*    cooking;

    static PhysX::PxMaterial* material;

    static PhysX::PxScene* scene;

    static PhysX::PxDefaultCpuDispatcher* cpu_dispatcher;
    static PhysX::PxDefaultGpuDispatcher* gpu_dispatcher;

    static Effect* effect;

  public:
    static void Initialize(Vector3 gravity);

    static PhysX::PxPhysics* GetPhysicsHandle();
    static PhysX::PxCooking* GetCookingHandle();

    static PhysX::PxRigidDynamic* CreateDynamic    (Vector3 position, Vector3 rotation, Shape& shape);
    static PhysX::PxRigidStatic*  CreateStatic     (Vector3 position, Vector3 rotation, Shape& shape);
    static PhysX::PxRigidDynamic* CreateKinematic  (Vector3 position, Vector3 rotation, Shape& shape);

    static void Add     (PhysX::PxRigidActor* actor);
    static void Remove  (PhysX::PxRigidActor* actor);

    static void Tick  (float dt);
    static void Draw  ();

    static void SetGravity(Vector3 gravity);
  };
}

#endif // !CORE_PHYSICS_H