//
// Created by M�rk�nen� on 23.10.2018
//

#ifndef CORE_RIGIDBODY_H
#define CORE_RIGIDBODY_H

#include <Component.h>
#include "Physics.h"

namespace Core
{
  enum FreezeFlags : int
  {
    FREEZE_POSITION_X = 1,
    FREEZE_POSITION_Y = 2,
    FREEZE_POSITION_Z = 4,

    FREEZE_ROTATION_X = 8,
    FREEZE_ROTATION_Y = 16,
    FREEZE_ROTATION_Z = 32
  };

  class Dynamic : public Component
  {
  private:
    PhysX::PxRigidDynamic* body;

  public:
    Dynamic(Actor*);

    void      SetVelocity (Vector3 velocity);
    Vector3   GetVelocity () const;

    void      SetAngularVelocity  (Vector3 velocity);
    Vector3   GetAngularVelocity  () const;

    void      SetPosition (Vector3 position);
    Vector3   GetPosition () const;

    void      SetRotation (Vector3 rotation);
    Vector3   GetRotation () const;

    void      SetFreezing (int flags);

    void Accelerate (Vector3 acceleration);
    void Push       (Vector3 force);
    void Hit        (Vector3 impulse);

    void Torque (Vector3 torque);

    virtual void OnTick         () override;
    virtual void OnPhysicsTick  () override;
    virtual void OnDestroy      () override;

    virtual Type GetType() override;

    PhysX::PxRigidActor* GetHandle() const;
  };
}

#endif // !CORE_RIGIDBODY_H