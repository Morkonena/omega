#include "Dynamic.h"
#include "Static.h"
#include "PhysX.h"
#include "Physics.h"

#include <Actor.h>
#include <Asset.h>
#include <Camera.h>
#include <Log.h>

using namespace Core;

PxDefaultAllocator* Physics::allocator;
PxDefaultErrorCallback* Physics::errors;
PxFoundation* Physics::foundation;
PxPhysics* Physics::physics;
PxCooking* Physics::cooking;
PxScene* Physics::scene;
PxMaterial* Physics::material;
PxDefaultCpuDispatcher* Physics::cpu_dispatcher;
Effect* Physics::effect;

void Physics::Initialize(Vector3 gravity)
{
  allocator = new PxDefaultAllocator();
  errors = new PxDefaultErrorCallback();
  foundation = PxCreateFoundation(PX_FOUNDATION_VERSION, *allocator, *errors);

  if (!foundation)
  {
    PERROR("ERROR_CREATE_PHYSICS_FOUNDATION");
    throw;
  }

  PxTolerancesScale tolerance;
  tolerance.length = 1; // Simulation is done in meters
  tolerance.mass = 1000; // Simulation is done in kilograms
  tolerance.speed = gravity.GetMagnitude(); // Gravity is recommended

  physics = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation, tolerance);

  if (!physics)
  {
    PERROR("ERROR_CREATE_PHYSICS");
    throw;
  }

  // Create the standard material
  material = physics->createMaterial(0.5f, 0.5f, 0.1f);

  PxCudaContextManagerDesc cuda_context_manager = {};
 
  
  PxCookingParams options(tolerance);
  options.buildGPUData = true;
  options.meshPreprocessParams = PxMeshPreprocessingFlags(PxMeshPreprocessingFlag::eWELD_VERTICES);
  options.meshWeldTolerance = 0.001f;

  // Create cooking to support convex shapes
  cooking = PxCreateCooking(PX_PHYSICS_VERSION, *foundation, options);
  
  if (!cooking)
  {
    PERROR("ERROR_CREATE_COOKING");
    throw;
  }

  PxSceneDesc scene_settings(tolerance);
  scene_settings.limits.maxNbActors = 1000;
  scene_settings.limits.maxNbBodies = 1000;
  scene_settings.limits.maxNbDynamicShapes = 1000;
  scene_settings.limits.maxNbStaticShapes = 1000;
  scene_settings.gravity = *(PxVec3*)&gravity;

  if (!scene_settings.cpuDispatcher)
  {
    cpu_dispatcher = PxDefaultCpuDispatcherCreate(4);

    if (!cpu_dispatcher)
    {
      PERROR("ERROR_CREATE_CPU_DISPATCHER");
    }

    scene_settings.cpuDispatcher = cpu_dispatcher;
  }

  scene_settings.filterShader = PxDefaultSimulationFilterShader;

  //sceneDesc.frictionType = PxFrictionType::eTWO_DIRECTIONAL;
  //sceneDesc.frictionType = PxFrictionType::eONE_DIRECTIONAL;
  scene_settings.flags |= PxSceneFlag::eENABLE_GPU_DYNAMICS;
  scene_settings.flags |= PxSceneFlag::eENABLE_PCM;
  //sceneDesc.flags |= PxSceneFlag::eENABLE_AVERAGE_POINT;
  scene_settings.flags |= PxSceneFlag::eENABLE_STABILIZATION;
  scene_settings.flags |= PxSceneFlag::eENABLE_ACTIVE_ACTORS;
  //sceneDesc.flags |= PxSceneFlag::eADAPTIVE_FORCE;
  scene_settings.flags |= PxSceneFlag::eENABLE_ACTIVETRANSFORMS;
  scene_settings.sceneQueryUpdateMode = PxSceneQueryUpdateMode::eBUILD_ENABLED_COMMIT_DISABLED;
  //sceneDesc.flags |= PxSceneFlag::eDISABLE_CONTACT_CACHE;
  scene_settings.broadPhaseType = PxBroadPhaseType::eGPU;
  scene_settings.gpuMaxNumPartitions = 8;

  // Create the main scene
  scene = physics->createScene(scene_settings);
  scene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 1.0f);
  scene->setVisualizationParameter(PxVisualizationParameter::eACTOR_AXES, 2.0f);
  scene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 2.0f);
  
  if(!scene)
  {
    PERROR("ERROR_CREATE_SCENE");
    throw;
  }

  SetGravity(gravity);

  Shader vs(SHADER_TYPE_VERTEX, Asset("Shaders/Lines/Shader.vs").GetText());

  if (!vs.IsValid())
  {
    PERROR("ERROR_LINES_VERTEX_SHADER: ", vs.GetLog());
    throw;
  }

  Shader fs(SHADER_TYPE_FRAGMENT, Asset("Shaders/Lines/Shader.fs").GetText());

  if (!fs.IsValid())
  {
    PERROR("ERROR_LINES_FRAGMENT_SHADER: ", fs.GetLog());
    throw;
  }
  
  effect = new Effect({ &vs, &fs }, 2);

  if (!effect->IsValid())
  {
    PERROR("ERROR_LINES_EFFECT: ", effect->GetLog());
    throw;
  }
  
  SUCCESS("Physics: OK");
}

PhysX::PxPhysics* Physics::GetPhysicsHandle()
{
  return physics;
}

PhysX::PxCooking* Physics::GetCookingHandle()
{
  return cooking;
}

PxTransform GetTransform(Vector3 position, Vector3 rotation)
{
  PxVec3& transform_position = *(PxVec3*)&position;
  PxQuat transform_rotation = PxQuat(rotation.x, PxVec3(1, 0, 0)) * PxQuat(rotation.y, PxVec3(0, 1, 0)) * PxQuat(rotation.z, PxVec3(0, 0, 1));

  return PxTransform(transform_position, transform_rotation);
}

PxRigidDynamic* Physics::CreateDynamic(Vector3 position, Vector3 rotation, Shape& shape)
{
  std::unique_ptr<PxGeometry> geometry = shape.GetGeometry();
  return PxCreateDynamic(*physics, GetTransform(position, rotation), *geometry, *material, shape.density, GetTransform(shape.position, shape.rotation));
}

PxRigidStatic* Physics::CreateStatic(Vector3 position, Vector3 rotation, Shape & shape)
{
  std::unique_ptr<PxGeometry> geometry = shape.GetGeometry();
  return PxCreateStatic(*physics, GetTransform(position, rotation), *geometry, *material, GetTransform(shape.position, shape.rotation));
}

PxRigidDynamic* Physics::CreateKinematic(Vector3 position, Vector3 rotation, Shape& shape)
{
  std::unique_ptr<PxGeometry> geometry = shape.GetGeometry();
  return PxCreateKinematic(*physics, GetTransform(position, rotation), *geometry, *material, shape.density, GetTransform(shape.position, shape.rotation));
}

void Physics::Add(PhysX::PxRigidActor* actor)
{
  scene->addActor(*actor);
}

void Physics::Remove(PhysX::PxRigidActor* actor)
{
  scene->removeActor(*actor);
}

void Physics::Tick(float dt)
{
  auto count = 0u;
  auto actors = scene->getActiveActors(count);

  for (auto i = 0u; i < count; i++)
  {
    Actor* actor = reinterpret_cast<Actor*>(actors[i]->userData);
    actor->OnPhysicsTick();
  }

  scene->simulate(dt);
  scene->fetchResults(true);
}

void Physics::Draw()
{
  const PxRenderBuffer& render_buffer = scene->getRenderBuffer();

  effect->Use();
  effect->Set("mvp", Camera::GetProjection() * Camera::GetView());

  Camera::SetLineThickness(1.0f);

  for (auto i = 0u; i < render_buffer.getNbLines(); i++)
  {
    const PxDebugLine& line = render_buffer.getLines()[i];
    effect->Set("line[0]", *reinterpret_cast<const Vector3*>(&line.pos0));
    effect->Set("line[1]", *reinterpret_cast<const Vector3*>(&line.pos1));

    Camera::Draw(DRAW_MODE_LINES, false, 2);
  }
}

void Physics::SetGravity(Vector3 gravity)
{
  scene->setGravity(*(PxVec3*)&gravity);
}

std::unique_ptr<PxGeometry> Box::GetGeometry()
{
  return std::make_unique<PxBoxGeometry>(*(PxVec3*)&extent);
}

std::unique_ptr<PxGeometry> Sphere::GetGeometry()
{
  return std::make_unique<PxSphereGeometry>(radius);
}

std::unique_ptr<PxGeometry> Capsule::GetGeometry()
{
  return std::make_unique<PxCapsuleGeometry>(radius, height / 2.0f);
}

std::unique_ptr<PxGeometry> Custom::GetGeometry()
{
  std::vector<unsigned int> indices(count);

  for (int i = 0; i < count; i++)
  {
    indices.push_back(i);
  }

  PxTriangleMeshDesc info;
  info.points.count = count;
  info.points.data = triangles;
  info.points.stride = sizeof(PxVec3);
  
  info.triangles.count = (int)indices.size() / 3;
  info.triangles.data = indices.data();
  info.triangles.stride = 3 * sizeof(unsigned int);

  PxTriangleMesh* mesh = Physics::GetCookingHandle()->createTriangleMesh(info, Physics::GetPhysicsHandle()->getPhysicsInsertionCallback());

  if (!mesh)
  {
    PERROR("ERROR_CREATE_CONVEX_MESH");
    throw;
  }

  return std::make_unique<PxTriangleMeshGeometry>(mesh);
}
   