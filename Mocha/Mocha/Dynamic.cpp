#include "PhysX.h"
#include "Dynamic.h"

#include <Actor.h>
#include <Vector2.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm\gtx\quaternion.hpp>

using namespace Core;

Vector3 ToRadians(PxQuat rotation)
{
  return *reinterpret_cast<Vector3*>(&glm::eulerAngles(*reinterpret_cast<glm::tquat<float>*>(&rotation)));
}

PxQuat ToQuaternion(Vector3 rotation)
{
  return *reinterpret_cast<PxQuat*>(&glm::tquat<float>(*reinterpret_cast<glm::vec3*>(&rotation)));
}

Dynamic::Dynamic(Actor* parent) : Component(parent)
{
  if (GetParent()->HasComponent<Shape>())
  {
    Shape* shape = GetParent()->GetComponent<Shape>();
    
    body = Physics::CreateDynamic(GetParent()->GetWorldPosition(), GetParent()->GetWorldRotation(), *shape);
    body->userData = parent;
    
    Physics::Add(body);
  }
}

void Dynamic::SetVelocity(Vector3 velocity)
{
  body->setLinearVelocity(*reinterpret_cast<PxVec3*>(&velocity));
}

Vector3 Dynamic::GetVelocity() const
{
  PxVec3 velocity = body->getLinearVelocity();
  return *reinterpret_cast<Vector3*>(&velocity);
}

void Dynamic::SetAngularVelocity(Vector3 velocity)
{
  body->setAngularVelocity(*reinterpret_cast<PxVec3*>(&velocity));
}

Vector3 Dynamic::GetAngularVelocity() const
{
  PxVec3 velocity = body->getAngularVelocity();
  return *reinterpret_cast<Vector3*>(&velocity);
}

void Dynamic::SetPosition(Vector3 position)
{
  PxTransform transform = body->getGlobalPose();
  transform.p = *reinterpret_cast<PxVec3*>(&position);

  body->setGlobalPose(transform);
}

Vector3 Dynamic::GetPosition() const
{
  PxVec3 position = body->getGlobalPose().p;
  return *reinterpret_cast<Vector3*>(&position);
}

void Dynamic::SetRotation(Vector3 rotation)
{
  PxTransform transform = body->getGlobalPose();
  transform.q = ToQuaternion(rotation);

  body->setGlobalPose(transform);
}

Vector3 Dynamic::GetRotation() const
{
  return ToRadians(body->getGlobalPose().q);
}

void Dynamic::SetFreezing(int flags)
{
  body->setRigidDynamicLockFlags((PxRigidDynamicLockFlags)flags);
}

void Dynamic::Accelerate(Vector3 acceleration)
{
  body->addForce(*reinterpret_cast<PxVec3*>(&acceleration), PxForceMode::eACCELERATION);
}

void Dynamic::Push(Vector3 force)
{
  body->addForce(*reinterpret_cast<PxVec3*>(&force));
}

void Dynamic::Hit(Vector3 impulse)
{
  body->addForce(*reinterpret_cast<PxVec3*>(&impulse), PxForceMode::eIMPULSE);
}

void Core::Dynamic::Torque(Vector3 torque)
{
  body->addTorque(*reinterpret_cast<PxVec3*>(&torque));
}

void Dynamic::OnTick()
{
  GetParent()->SetWorldPosition(GetPosition());
  GetParent()->SetWorldRotation(GetRotation());
}

void Dynamic::OnPhysicsTick()
{
  Vector3 position = GetParent()->GetWorldPosition();
  Vector3 rotation = GetParent()->GetWorldRotation();

  //SetPosition(position);
  //SetRotation(rotation);
}

void Dynamic::OnDestroy()
{
  Physics::Remove(body);
}

Type Dynamic::GetType()
{
  return typeid(Dynamic);
}

PhysX::PxRigidActor* Dynamic::GetHandle() const
{
  return body;
}
