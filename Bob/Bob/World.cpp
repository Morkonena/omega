#include "Cell.h"
#include "World.h"

#include <algorithm>

#include <Camera.h>
#include <Standard.h>
#include <Pool.h>

using namespace Core;

std::map<Vector2, std::unique_ptr<Cell>, std::function<bool(const Vector2&, const Vector2&)>> world_cells;

void World::Initialize()
{
  world_cells = std::map<Vector2, std::unique_ptr<Cell>, std::function<bool(const Vector2&, const Vector2&)>>([](const Vector2& a, const Vector2& b)
  {
    return (b.y < a.y) || (b.y == a.y && b.x < a.x);
  });
}

Cell* CreateCell(Vector2 location)
{
  Cell* cell = new Cell(location);
  world_cells.insert(std::make_pair(location, std::move(std::unique_ptr<Cell>(cell))));

  cell->Generate();

  return cell;
}

class Bundle
{
private:
  mutable std::vector<Task*> tasks;

public:
  Bundle(Vector2 origin, int extent);
  bool IsLoaded() const;
};

Bundle* bundle = nullptr;

Bundle::Bundle(Vector2 origin, int extent)
{
  int load_extent = extent + 1;
  
  // Destroy all unnecessary cells
  for (auto i = world_cells.begin(); i != world_cells.end();)
  {
    Cell& cell = *i->second;

    Vector2 location = cell.GetLocation();
    Vector2 delta = origin - location;

    // Cells are destoryed when they are outside the load distance
    if (abs(delta.x) > load_extent || abs(delta.y) > load_extent)
    {
      // Erase the cell from the list and continue
      i = world_cells.erase(i);
    }
    else
    {
      i++;
    }
  }

  std::vector<Cell*> triangulation_list;

  for (int y = -load_extent; y <= load_extent; y++)
  {
    for (int x = -load_extent; x <= load_extent; x++)
    {
      Vector2 location = origin + Vector2(x, y);
      Cell* cell = World::GetCellAt(location);

      if (cell == nullptr)
      {
        // Generate a new cell
        cell = CreateCell(location);
      }
      // Cell won't be triangulated if it's already triangulated, obviously
      else if (cell->IsTriangulated())
      {
        continue;
      }

      // Triangulate the cell only if it's inside the render distance
      if (abs(x) <= extent && abs(y) <= extent)
      {
        triangulation_list.push_back(cell);
      } 
    }
  }

  // Triangulate all cells in the list
  for (Cell* cell : triangulation_list)
  {
    Task* task = Pool::Run([=]() { cell->Triangulate(); });
    tasks.push_back(task);
  }
}

bool Bundle::IsLoaded() const
{
  bool l = true;

  for (auto i = tasks.begin(); i != tasks.end();)
  {
    Task* task = *i;

    if (task->IsDone())
    {
      i = tasks.erase(i);
    }
    else
    {
      i++;
      l = false;
    }
  }

  return l;
}

void World::Tick()
{
  if (bundle == nullptr || bundle->IsLoaded())
  {
    bundle = new Bundle(GetActiveCell(), 10);
  }

  for (auto& i : world_cells)
  {
    (i.second)->Tick();
  }
}

#define CELL(X, Z) Vector2(floorf(X / (float)CELL_WIDTH), floorf(Z / (float)CELL_LENGTH))

bool World::Exists(int x, int y, int z)
{
  if (y < 0 || y >= CELL_HEIGHT)
  {
    return false;
  }

  Vector2 location = CELL(x, z);
  Cell* cell = GetCellAt(location);

  if (cell != nullptr)
  {
    Vector3 local = Vector3(x, y, z) - cell->GetWorldLocation();
    return cell->Exists(local);
  }
  
  return false;
}

Vector2 World::GetActiveCell()
{
  return CELL(Camera::position.x, Camera::position.z);
}

Cell* World::GetCellAt(Vector2 location)
{
  auto i = world_cells.find(location);
  return (i == world_cells.end()) ? nullptr : i->second.get();
}

void World::Reset()
{
  world_cells.clear();
}
