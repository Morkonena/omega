#include "Instance.h"
#include "Time.h"

#define PRESS_ANY_KEY std::getchar()

int main()
{
	std::unique_ptr<Instance> instance;

	try
	{
		instance = std::make_unique<Instance>(2);
	}
	catch (...)
	{
		PERROR("PRESS ANY KEY TO EXIT");
		PRESS_ANY_KEY;
		return 1;
	}

	auto start = NOW;
	auto delta = 0.0f;

	while (!instance->GetWindow().IsClosing())
	{
		delta = MILLISECONDS(SINCE(start)) / 1000.0f;
		start = NOW;

		instance->Update(delta);
	}

	return 0;
}