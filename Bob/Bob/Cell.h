//
// Created by Nogey on 7.10.2018
//

#ifndef CORE_CELL_H
#define CORE_CELL_H

// #include <DiffuseMaterial.h>
#include <Vector2.h>
#include "MarchingCubes.h"

#define CELL_EXTENT 16
#define CELL_WIDTH CELL_EXTENT
#define CELL_LENGTH CELL_WIDTH
#define CELL_HEIGHT (CELL_EXTENT * 4)

namespace Core
{
  class Cell
  {
  private:
    Vector2 location;

    std::vector<bool> states;
    bool triangulated = false;

    std::unique_ptr<MarchingCubes::Shape> shape;

  public:
    Cell(Vector2 location);

    void Generate();
    void Triangulate();

    bool IsTriangulated() const;

    void Tick();

    Vector3 GetLocation() const;
    Vector3 GetWorldLocation() const;

    bool Exists(Vector3 local) const;

    MarchingCubes::Shape& GetShape();
  };
}

#endif // !CORE_CELL_H