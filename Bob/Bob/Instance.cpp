#include "Instance.h"
#include "Entry.h"

Instance::Instance(int threads)
{
	Core::Console::Initialize();
	Core::Graphics::Initialize();

	window = std::make_unique<Core::Window>(Core::Screen::GetWidth(), Core::Screen::GetHeight(), true);

	Core::Assets::Initialize();
	Core::Pool::Initialize(window.get(), threads);

	window->Start();
	window->SetCursorVisible(false);

	Core::DefferedRenderer::Initialize(*window);
	Core::SpecularMaterial::Initialize();
	Core::DiffuseMaterial::Initialize();

	Core::DefferedRenderer::Register<Core::SpecularMaterial>();

	Core::Fbx::Initialize();
	
	Core::Camera::Initialize();
	Core::Camera::SetProjection(Core::Matrix::Perspective(45, Core::Screen::GetAspect(), 0.01f, 1000.0f));
	Core::Camera::position = Core::Vector3(0, 0, 0);
	Core::Camera::SetWindingOrder(false);

	Core::Physics::Initialize(Core::Vector3(0, 0, 0));

	Entry::Start(*this);
}

Core::Window& Instance::GetWindow()
{
	return *window;
}

void Instance::Update(float dt)
{
	Core::Physics::Tick(dt);

	Entry::Update(*this, dt);
	Core::DefferedRenderer::Render();
	
	window->Update();
}
