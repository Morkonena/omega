//
// Created by Nogey on 7.10.2018
//

#ifndef CORE_WORLD_H
#define CORE_WORLD_H

#include <Vector2.h>
#include "Cell.h"

namespace Core
{
  namespace World
  {
    void Initialize();

    Vector2 GetActiveCell();

    void Tick();
    bool Exists(int x, int y, int z);

    Cell* GetCellAt(Vector2 location);

    void Reset();
  }
}

#endif // !CORE_WORLD_H