//
// Created by Nogey on 2.9.2018
//

#ifndef CORE_MAP_H
#define CORE_MAP_H

#include <Standard.h>
#include <functional>

namespace Core
{
  template<typename Key, typename Value>
  struct Node
  {
    Node* less = nullptr;
    Key key;
    Value value;
    Node* greater = nullptr;

    inline Node(Key& key, Value& value)
    {
      this->key = key;
      this->value = value;
    }
  };

  template<typename Key>
  using Comparator = std::function<bool(const Key&, const Key&)>;

  template<typename Key, typename Value>
  class Map
  {
  private:
    List<Node<Key, Value>*> nodes;
    Comparator<Key> comparator;

  public:
    Map(Comparator<Key>& comparator);

    void Add(const Key& key, const Value& value);
    void Remove(const Key& key);

    Value* Get(const Key& key) const;
    Value* GetAt(int i) const;

    int GetSize() const;

    void Clear();
  };

  template<typename Key, typename Value>
  inline Map<Key, Value>::Map(Comparator<Key>& comparator)
  {
    this->comparator = comparator;
  }

  template<typename Key, typename Value>
  inline void Map<Key, Value>::Add(const Key& key, const Value& value)
  {
    Node<Key, Value>* node = nodes[0];

    while (true)
    {
      if (node->key == key)
      {
        throw;
      }

      Node<Key, Value>** next = comparator(key, node->key) ? &node->less : &node->greater;

      if (*next == nullptr)
      {
        *next = new Node<Key, Value>(key, value);
        nodes.push_back(*next);
        return;
      }
      else
      {
        node = *next;
      }
    }
  }

  template<typename Key, typename Value>
  inline void Map<Key, Value>::Remove(const Key& key)
  {

  }

  template<typename Key, typename Value>
  inline Value* Map<Key, Value>::Get(const Key& key) const
  {
    Node<Key, Value>* node = nodes[0];

    while (node)
    {
      if (node->key == key)
      {
        return &node->value;
      }

      if (comparator(key, node->key))
      {
        node = node->less;
      }
      else
      {
        node = node->greater;
      }
    }

    return nullptr;
  }

  template<typename Key, typename Value>
  inline Value* Map<Key, Value>::GetAt(int i) const
  {
    return;
  }

  template<typename Key, typename Value>
  inline int Map<Key, Value>::GetSize() const
  {
    return nodes.size();
  }

  template<typename Key, typename Value>
  inline void Map<Key, Value>::Clear()
  {
    for each (Node<Key, Value>*& node in nodes)
    {
      delete node;
    }

    nodes.clear();
  }
}

#endif // !CORE_MAP_H