//
// Created by M�rk�nen� on 5.11.2018
//

#ifndef ENTRY_H
#define ENTRY_H

#include "Instance.h"

class Entry
{
public:
	static void Start(Instance& instance);
	static void Update(Instance& instance, float dt);
};

#endif // !ENTRY_H