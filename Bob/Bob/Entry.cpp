#include "Entry.h"
#include <Buffer.h>
#include <random>

Core::Actor* floor_;
Core::Actor player;

Core::Actor cube_;

Core::Actor* monkey;
Core::Actor* torus;

std::vector<Core::Actor*> meteors;

void Entry::Start(Instance& instance)
{
	monkey = Core::Fbx::Import("Monkey.fbx");
	monkey->position += Core::Vector3(0, 200, 0);
	monkey->GetChildAt(0)->rotation = Core::Vector3();
	monkey->GetChildAt(0)->GetComponent<Core::SpecularMaterial>()->SetPower(100);
	
	torus = Core::Fbx::Import("Torus.fbx");
	torus->position += Core::Vector3(0, 200, 0);
	torus->GetChildAt(0)->rotation = Core::Vector3();
	torus->position += Core::Vector3(300.0f, 0, 0);
	
	Core::Mesh* mesh = torus->GetChildAt(0)->GetComponent<Core::SpecularMaterial>()->GetMesh();
	torus->GetChildAt(0)->GetComponent<Core::SpecularMaterial>()->SetPower(500);
	
	Core::Capsule* capsule = player.AddComponent<Core::Capsule>();
	capsule->density = 10;
	capsule->radius = 0.5f;
	capsule->height = 1.5f;
	
	Core::Dynamic* dynamic_body = player.AddComponent<Core::Dynamic>();
	dynamic_body->SetFreezing(Core::FREEZE_ROTATION_X | Core::FREEZE_ROTATION_Y | Core::FREEZE_ROTATION_Z);
	
	Core::Physics::SetGravity(Core::Vector3(0, -9.81f, 0));
	
	Core::Actor* model = Core::Fbx::Import("Cube.fbx");
	Core::Actor* cube = model->GetChildAt(0);
	cube->SetParent(nullptr);
	
	floor_ = cube;
	floor_->position = Core::Vector3(0, -20, 0);
	floor_->rotation = Core::Vector3();
	floor_->scale = Core::Vector3(100, 1, 100);
	
	Core::Box* box = floor_->AddComponent<Core::Box>();
	box->extent = Core::Vector3(100, 1, 100);
	
	floor_->AddComponent<Core::Static>();
	
	cube_.position = Core::Vector3(0, 100, 0);
	cube_.AddComponent<Core::SpecularMaterial>()->SetMesh(cube->GetComponent<Core::SpecularMaterial>()->GetMesh());
	box = cube_.AddComponent<Core::Box>();
	box->density = 10;
	box->extent = Core::Vector3(0.5f);
	cube_.AddComponent<Core::Dynamic>();

	std::random_device device;
	std::mt19937 twister(device());
	std::uniform_real_distribution<float> random(-50, 50);

	for (int i = 0; i < 100; i++)
	{
		Core::Actor* meteor = new Core::Actor();
		meteor->position = Core::Vector3(random(twister), 100, random(twister));

		Core::SpecularMaterial* material = meteor->AddComponent<Core::SpecularMaterial>();

		Core::Box* box = meteor->AddComponent<Core::Box>();
		box->density = 2;
		box->extent = Core::Vector3(0.5f);

		Core::Dynamic* dynamic = meteor->AddComponent<Core::Dynamic>();

		material->SetMesh(monkey->GetChildAt(0)->GetComponent<Core::SpecularMaterial>()->GetMesh());
		material->SetPower(200);

		meteors.push_back(meteor);
	}
}

void Move(Instance& instance, float dt)
{
	

	Core::Window& window = instance.GetWindow();

	Core::Vector2 motion = window.GetMouseMovement();
	Core::Camera::rotation += Core::Vector3(motion.y, motion.x, 0) * dt;

	Core::Matrix rotation = Core::Matrix::Rotate(Core::Camera::rotation);

	Core::Vector3 forward = (Core::Vector3)(rotation * Core::Vector4(0, 0, 1, 1));
	Core::Vector3 right = (Core::Vector3)(rotation * Core::Vector4(1, 0, 0, 1));
	Core::Vector3 movement;

	if (window.IsKeyDown('W'))
	{
		movement += forward;
	}

	if (window.IsKeyDown('S'))
	{
		movement -= forward;
	}

	if (window.IsKeyDown('A'))
	{
		movement -= right;
	}

	if (window.IsKeyDown('D'))
	{
		movement += right;
	}

	movement.y = 0;
	movement.Normalize();

	Core::Vector3 velocity = player.GetComponent<Core::Dynamic>()->GetVelocity();
	velocity = Core::Vector3(0, velocity.y, 0);

	player.GetComponent<Core::Dynamic>()->SetVelocity(velocity + movement * 10);
}

void Entry::Update(Instance& instance, float dt)
{
	Core::Camera::position = player.position + Core::Vector3(0, 0.5f, 0);
	Move(instance, dt);
	floor_->OnTick();
	cube_.OnTick();
	player.OnTick();
	monkey->OnTick();
	torus->OnTick();

	for (Core::Actor* meteor : meteors)
	{
		meteor->OnTick();
	}
}