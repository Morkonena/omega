#include "Cell.h"

#include <perlin\noise.hpp>

#include <DiffuseMaterial.h>
#include <Pool.h>

#include <Log.h>

using namespace Core;
using namespace Math;

Cell::Cell(Vector2 location)
{
  this->location = location;
  this->states.resize(CELL_WIDTH * CELL_HEIGHT * CELL_LENGTH);
}

#define FREQUENCY 0.010
#define ROUGHNESS 2.15

#define i(X, Y, Z) ((int(Z) * int(CELL_WIDTH) * int(CELL_HEIGHT)) + (int(Y) * int(CELL_WIDTH)) + int(X))

void Cell::Generate()
{
  Perlin perlin;
  Vector3 world_location = GetWorldLocation();

  for (int z = 0; z < CELL_LENGTH; z++)
  {
    for (int x = 0; x < CELL_WIDTH; x++)
    {
      //double height = perlin.octaveNoise0_1((world_location.x + x) * FREQUENCY, (world_location.z + z) * FREQUENCY, 5);
      //height = pow(height, ROUGHNESS);
      //height = height * CELL_HEIGHT;

      double height = perlin.noise0_1((world_location.x + x) * FREQUENCY, (world_location.z + z) * FREQUENCY) * CELL_HEIGHT;

      for (int y = 0; y < int(height); y++)
      {
        states[i(x, y, z)] = true;
      }
    }
  }
}

void Cell::Triangulate()
{
  // Generate marching cubes based geometry
  Vector3 location = GetWorldLocation();
  Vector3 extent(CELL_WIDTH, CELL_HEIGHT, CELL_LENGTH);

  shape = std::unique_ptr<MarchingCubes::Shape>(MarchingCubes::Generate(location, extent));
  shape->SetLocation(location);

  triangulated = true;
}

bool Cell::IsTriangulated() const
{
  return triangulated;
}

void Cell::Tick()
{
  if (IsTriangulated())
  {
    shape->Tick();
  }
}

Vector3 Cell::GetLocation() const
{
  return location;
}

Vector3 Cell::GetWorldLocation() const
{
  return Vector3(location.x * CELL_EXTENT, 0, location.y * CELL_EXTENT);
}

bool Cell::Exists(Vector3 local) const
{
  return states[i(local.x, local.y, local.z)];
}

MarchingCubes::Shape& Cell::GetShape()
{
  return *shape;
}
