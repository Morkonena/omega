//
// Created by Nogey on 22.7.2018
//

#ifndef MARCHING_CUBES_H
#define MARCHING_CUBES_H

#include <Actor.h>
#include <Buffer.h>
#include <Mesh.h>
#include <Standard.h>
#include <Vector3.h>

namespace Core
{
  namespace MarchingCubes
  {
    class Bucket;

    struct Triangle
    {
    private:
      Bucket* bucket;
      Vector3* vertices;
      Vector3* normals;

    public:
      Triangle(Bucket* bucket, Vector3* vertices, Vector3* normals);

      void SetVertex(int i, const Vector3& position);
      void SetNormal(int i, const Vector3& normal);

      Vector3 GetVertex(int i) const;
      Vector3 GetNormal(int i) const;
    };

    struct Cube
    {
    private:
      Bucket* bucket;
      Vector3* vertices;
      Vector3* normals;
      //int base;

    public:
      Cube(Bucket* bucket, Vector3* vertices, Vector3* normals);

      Triangle GetTriangle(int i);

      inline constexpr int GetTriangleCount()
      {
        return 5;
      }
    };

    class Bucket
    {
    private:
      std::map<Vector3, short, std::function<bool(const Vector3&, const Vector3&)>> positions;
      
      Mesh mesh;
      Actor actor;

      Vector3* mapped_vertices;
      Vector3* mapped_normals;

      int capacity;
      int components;
      int position = 0;

      bool is_uploaded = false;

    public:
      Bucket(int capacity);

      void Tick();

      Cube Create(int x, int y, int z);
      Cube Get(int x, int y, int z);

      bool Contains(int x, int y, int z) const;

      int GetAvailable() const;

      void SetLocation(Vector3 location);
    };

    class Shape
    {
      Vector3 extent;
      Actor actor;

      std::vector<std::unique_ptr<Bucket>> buckets;

      Bucket* CreateBucket();

    public:
      Shape();
      Shape(Vector3 extent);

      void Tick();
      void SetLocation(Vector3 location);

      Cube AddCube(int x, int y, int z);
    };

    Shape* Generate(const Vector3& start, const Vector3& extent);
  }
}

#endif // !MARCHING_CUBES_H