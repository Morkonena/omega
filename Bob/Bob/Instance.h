//
// Created by M�rk�nen� on 2.11.2018
//

#ifndef INSTANCE_H
#define INSTANCE_H

#include <Actor.h>
#include <Asset.h>
#include <Camera.h>
#include <Console.h>
#include <DefferedRenderer.h>
#include <DiffuseMaterial.h>
#include <Dynamic.h>
#include <Fbx.h>
#include <Framebuffer.h>
#include <Graphics.h>
#include <Log.h>
#include <Pool.h>
#include <Physics.h>
#include <Screen.h>
#include <SpecularMaterial.h>
#include <Static.h>
#include <Texture.h>
#include <Window.h>
#include <Windows.h>

class Instance
{
private:
	std::unique_ptr<Core::Window> window;

public:
	Instance(int threads = -1);

	Core::Window& GetWindow();

	void Update(float dt);
};

#endif // !INSTANCE_H