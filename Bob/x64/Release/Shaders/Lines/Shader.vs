#version 450

uniform mat4 mvp;
uniform vec3 line[2];

void main ()
{
	gl_Position = mvp * vec4(line[gl_VertexID], 1.0);
}