#version 450
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 color;

uniform mat4 mvp;
uniform mat4 m;

out vec4 vposition;
out vec2 vuv;
out vec3 vnormal;
out vec3 vcolor;

void main ()
{
	mat3 r = mat3(m);

	vposition = m * vec4(vertex, 1.0);
	vuv = uv;
	vnormal = normalize(r * normal);
	vcolor = color;
   
	gl_Position = mvp * vec4(vertex, 1.0);
}