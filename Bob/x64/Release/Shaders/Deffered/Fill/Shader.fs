#version 450
precision highp float;

layout(location=0) out int omid;
layout(location=1) out vec3 oposition;
layout(location=2) out vec3 onormal;
layout(location=3) out vec3 ocolor;

in vec4 vposition;
in vec2 vuv;
in vec3 vnormal;
in vec3 vcolor;

uniform int mid;
uniform sampler2D sampler;

void main ()
{
	vec3 tc = texture(sampler, vuv).rgb;
	omid = mid;
	oposition = vposition.rgb;
	onormal = vnormal;
	ocolor = vcolor;
}