#version 450
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;

out vec2 vuv;

void main ()
{
   vuv = uv;
   gl_Position = vec4(vertex, 1.0);
}