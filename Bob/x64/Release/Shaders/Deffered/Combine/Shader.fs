#version 450
precision highp float;

uniform sampler2DArray layers;
uniform int layer_count;

in vec2 vuv;
out vec4 ocolor;

void main ()
{
	int i = 0;
	
	while (i < layer_count)
	{
		ocolor = texture(layers, vec3(vuv, i));
		
		if (ocolor.r != 0 || ocolor.g != 0 || ocolor.b != 0 || ocolor.a != 0)
		{
			break;
		}
		
		i++;
	}
}