#version 450
precision highp float;



//----------------------------------------------------------------------------------------------------



struct Material
{
	vec3 diffuse;
	vec3 specular;
	float power;
};

struct Pixel
{
	vec3 position;
	vec3 normal;
	vec3 color;
	
	Material material;
};


//----------------------------------------------------------------------------------------------------



uniform vec3 	ambient;
uniform float 	ambient_ratio;

uniform vec3 dl_direction;
uniform vec3 dl_position;

uniform vec3 camera_position;



//----------------------------------------------------------------------------------------------------



vec4 GetDiffuse (vec3 color, vec3 normal)
{
	float intensity = (dot(dl_direction, -normal) + 1.0) / 2.0;
	intensity = max(intensity, 0.2);
	
	return vec4(mix(color, ambient, ambient_ratio) * intensity, 1.0);
}

vec4 GetSpecular (vec3 position, vec3 normal, vec3 specular, float shininess)
{
	vec3 reflection = normalize(reflect(dl_direction, normal));
	vec3 vtoc = normalize(camera_position - position);
	
	float intensity = (dot(vtoc, reflection) + 1.0) / 2.0;
	intensity = max(intensity, 0.2);
	
	return vec4(specular * pow(intensity, shininess), 1.0);
}

vec4 Calculate (Pixel pixel)
{
	vec4 color = GetDiffuse(vec3(0.8, 0.8, 0.8), pixel.normal);
	     color = normalize(color);
		 
	Material material = pixel.material;
	
	if (material.power > 0.0)
	{
		color += GetSpecular(pixel.position, pixel.normal, material.specular, material.power);
	}
	
	color.w = 1.0;
	
	return color;
}



//----------------------------------------------------------------------------------------------------

layout(std430) buffer MaterialBuffer
{
	Material Materials[];
};

uniform sampler2D materials;
uniform sampler2D positions;
uniform sampler2D normals;
uniform sampler2D colors;

in vec2 vuv;
out vec4 ocolor;

void main ()
{
	int id = int(texture(materials, vuv).r);
	
	Pixel pixel;
	pixel.position 	= texture(positions, vuv).rgb;
	pixel.normal 	= texture(normals, 	 vuv).rgb;	
	pixel.color 	= texture(colors, 	 vuv).rgb;
	pixel.material  = Materials[id];
	
	pixel.normal = normalize(pixel.normal);
	
	ocolor = Calculate(pixel);
}